#!/usr/bin/env python3
from clean_data.constants import PruneNames as PNames
from clean_data.example_data import examples
from clean_data import time
import numpy as np
import pandas as pd

'''
Tests for `clean_data.time`
'''
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONSTANTS
DATA1='crude_follow_up_data'
FOLLOW='follow-up'
FINAL_TIME='time_till_death'
AFIB_TIME='time_till_afib'
DEATH='All-cause mortality'
COL_NAMES={
    'enrolment_date': 'start', 'last_date_of_contact': 'stop',
    'afib_date': 'event', 'death_date': 'death'}
FOLLOW_UP={'follow-up': ['enrolment_date', 'last_date_of_contact'],}
EVENT_TIME={
    'time_till_afib': ['enrolment_date', 'afib_date'],
    'time_till_death': ['enrolment_date', 'death_date'],}
DATE_FORMAT='%d-%m-%Y'
DATE_COLUMNS = {'enrolment_date': ['2013-02-15', '2012-04-23', '2013-01-28'],
                'last_date_of_contact': ['2019-02-15', '2018-09-14',
                                         '2019-03-15'],
                'afib_date death_date': ['2019-02-10', '2014-04-10',
                                         '2019-01-25',],
                'follow-up': [2191.0, 2335.0, 2237.0],
                'time_till_afib': [2186.0, 717.0, 2188.0],
                'time_till_death': [2187.0, 1066.0, 1849.0],
                }

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestCreate_Follow_Up(object):
    '''
    Testing 'time.create_follow_up`.
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_follow_up_time(self):
        # getting data
        data = examples.get_data(DATA1)
        # results
        obj = time.create_follow_up(data=data, date_dict=COL_NAMES,
                                    follow_up=FOLLOW_UP,
                                    follow_up_drop_zero=False,
                                    format=DATE_FORMAT,
                                    )
        # assert
        NAMES = list(COL_NAMES.keys())
        assert obj.data[NAMES[0]].name == NAMES[0]
        assert obj.data[FOLLOW].to_list()[:-1] == \
            [3137.0, 2191.0, 2335.0, 829.0, 2237.0]
        assert np.isnan(obj.data[FOLLOW].to_list()[-1])
        assert obj.rows_with_non_positive_follow_up == []
        assert obj.rows_with_non_positive_event_time == []
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_event_time(self):
        # getting data
        data = examples.get_data(DATA1)
        # results
        obj = time.create_follow_up(data=data, date_dict=COL_NAMES,
                                    follow_up=FOLLOW_UP,
                                    event_time=EVENT_TIME,
                                    follow_up_drop_zero=False,
                                    event_time_drop_zero=False,
                                    format=DATE_FORMAT,
                                    )
        # assert
        NAMES = list(EVENT_TIME.keys())
        assert obj.data[NAMES[0]].name == NAMES[0]
        assert obj.data[AFIB_TIME].to_list() == \
            [4423.0, 2186.0, 717.0, 829.0, 2188.0, 1.0]
        assert obj.rows_with_non_positive_follow_up == []
        assert obj.rows_with_non_positive_event_time == []
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_drop(self):
        # getting data
        data = examples.get_data(DATA1)
        # results
        obj = time.create_follow_up(data=data, date_dict=COL_NAMES,
                                    follow_up=FOLLOW_UP,
                                    event_time=EVENT_TIME,
                                    follow_up_drop_zero=True,
                                    event_time_drop_zero=True,
                                    format=DATE_FORMAT,
                                    )
        # assert
        NAMES = list(EVENT_TIME.keys())
        assert obj.data[NAMES[0]].name == NAMES[0]
        assert obj.data[AFIB_TIME].to_list() == \
            [2186.0, 717.0, 2188.0]
        assert obj.rows_with_non_positive_follow_up == [5]
        assert obj.rows_with_non_positive_event_time == [3,0]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestCorrect_Time(object):
    '''
    Testing 'time.correct_time`.
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_correct_time(self):
        data = examples.get_data(DATA1).iloc[[1,2,4]]
        # add columns
        data = pd.concat(
            [data, pd.DataFrame(DATE_COLUMNS).set_index(data.index)], axis=1,
        )
        # run function
        data = time.correct_time(data=data,
                                 test_time_col=FOLLOW, max_time_col=FINAL_TIME,
                                 correct_time_col=FINAL_TIME,
                                 dependent_time_columns=[AFIB_TIME, ],
                                 verbose=False,
                                )
        # assert
        assert data[FOLLOW].to_list() == [2187.0, 1066.0, 1849.0]
        assert data[AFIB_TIME].to_list() == [2186.0, 717.0, 1849.0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestTruncated_event_indicator(object):
    '''
    Testing 'time.truncated_event_indicator`.
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_truncated_event_indicator(self):
        data = examples.get_data(DATA1).iloc[[1,2,4]]
        # add columns
        data = pd.concat(
            [data, pd.DataFrame(DATE_COLUMNS).set_index(data.index)], axis=1,
        )
        MAX_FU_TIME=2000
        # add events which did not yet occurred
        data['event_bin'] = [1,0,1]
        data = time.truncated_event_indicator(data=data, time_col=AFIB_TIME,
                                              event_col='Afib',
                                              event_indicator_all_time='event_bin',
                                              time_threshold = MAX_FU_TIME,
                                              event_target_value = [1],
                                              )
        # assert
        assert data['Afib'].isnull().to_list() == [False, True, False]
