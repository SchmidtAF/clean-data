#!/usr/bin/env python3
import tempfile

from clean_data.constants import FormatTabular as TableNames
from clean_data.example_data import examples
from clean_data import (
    format_tabular,
)
from openpyxl import load_workbook
import numpy as np

'''
Tests for `clean_data.format_tabular`
'''


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestNlog10_Func(object):
    '''
    Testing functions for the `nlog10_func` function.
    '''
    
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_nlog10_func(self):
        # input and output
        values = [0.01, 0.4, 0.02, 0, 0.00000000000000000000000000000001]
        expected1 = [2.0, 0.3979400086720376, 1.6989700043360187, 20, 20]
        expected2 = [1.5, 0.3979400086720376, 1.5, 1.5, 1.5]
        # run
        results1 = format_tabular.nlog10_func(values)
        results2 = format_tabular.nlog10_func(values, max_log10=1.5)
        # assert
        assert results1 == expected1
        assert results2 == expected2



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Test_Sort_Table(object):
    '''
    Testing functions for the `sort_table` function.
    '''

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_sort_table(self):
        # getting data
        dict_test = {'name': ['test1', 'test3', 'test4', 'test2'],
                     'value': [1, 3, 4, 2],
                     }
        data_test = format_tabular.sort_table(dict_test,
                                              sort_column='value',
                                              kwargs_sort_value={
                                                  'ascending': False
                                              },
                                              )
        assert data_test['value'].to_list() == [4, 3, 2, 1]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Test_Standardise_Table(object):
    '''
    Testing functions for the `Standardise_Table` class.
    '''
    
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_calc_minus_log10(self):
        # Getting the data
        table = examples.load_table_data()
        # running function
        table2 = format_tabular.Standardise_Table(table)
        table2.calc_minus_log10(original_columns=['ld'])
        assert np.round(table2.data['ld'].iloc[0], 3) == 0.398
        # running function
        table3 = format_tabular.Standardise_Table(table)
        table3.calc_minus_log10(
            original_columns=['ld'], nlog10_kwargs={'max_log10': 0.2},
        )
        assert table3.data['ld'].iloc[0] == 0.2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_apply_function(self):
        func_test = lambda x: x + 2
        # Getting the data
        table = examples.load_table_data()
        table2 = format_tabular.Standardise_Table(table)
        # run function
        table2.apply_function(func_columns=['ld'],
                              func=func_test, )
        assert table2.data['ld'].iloc[0] == 2.4
        # run function
        table3 = format_tabular.Standardise_Table(table)
        table3.apply_function(func_columns=['ld'],
                              new_columns=['ld2'],
                              func=func_test, )
        assert table3.data['ld2'].iloc[0] == 2.4
        assert table3.data['ld'].iloc[0] == 0.4
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_move_to_front(self):
        table = examples.load_table_data()
        table2 = format_tabular.Standardise_Table(table)
        # run algorithm
        table2.move_to_front(columns=['ld', 'model'])
        assert list(table2.data.columns[0:2]) == ['ld', 'model']
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_change_column_values(self):
        table = examples.load_table_data()
        table2 = format_tabular.Standardise_Table(table)
        # run algorithm
        table2.change_column_values(
            column_values_dict={'outcome': {
                'hdl_glgc': 'HDL-C (GLGC)',
                'tg_glgc': 'TG (GLGC)'},
                'exposure': {'SAMP_2474_54_5': 'SAMP'}}
        )
        assert list(table2.data['outcome'][1:3]) == ['HDL-C (GLGC)',
                                                     'TG (GLGC)']
        assert table2.data['exposure'][0] == 'SAMP'
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_change_column_values(self):
        table = examples.load_table_data()
        table2 = format_tabular.Standardise_Table(table)
        # run algorithm
        table2.data.loc['ldl_glgc_LD_0.4_pruned', 'outcome'] = '  test '
        table2.strip_column_values(columns=['index', 'outcome'])
        assert table2.data.loc['ldl_glgc_LD_0.4_pruned', 'outcome'] == 'test'
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_rename_columns(self):
        table = examples.load_table_data()
        # run algorithm
        table2 = format_tabular.Standardise_Table(table)
        table2.rename_columns(original_columns=['se', 'pvalue'],
                              new_columns=['standard_error', 'P-value'])
        assert set(['standard_error', 'P-value']).issubset(set(
            table2.data.columns.to_list()))
        # run algorithm
        table2 = format_tabular.Standardise_Table(table)
        table2.rename_columns(
            original_columns=['point', 'se', 'pvalue'],
            new_columns=['effect_estimate', 'standard_error', 'P-value'],
            drop_original=True)
        assert set(['standard_error', 'P-value']).issubset(set(
            table2.data.columns.to_list()))
        assert set(['point', 'se']).issubset(set(
            table2.data.columns.to_list())) == False

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Test_WriteSupplementaryTables(object):
    '''
    Testing functions for the `write_supplementary_tables` class.
    '''
    
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_write_supplementary_tables(self):
        table_1 = examples.patient_characteristics(10)
        table_2 = examples.patient_characteristics(15, 2, 2, 2)
        tables = {'table_1_description': table_1,
                  'table_2_description': table_2}

        with tempfile.NamedTemporaryFile(suffix='.xlsx') as tmp:
            tmp_file_path = tmp.name
            format_tabular.write_supplementary_tables(
                tables,
                output_file_path=tmp_file_path,
                sheet_prefix='S',
                index=False)
            wb = load_workbook(tmp_file_path)
            # Test sheet names
            assert wb.sheetnames == ['Descriptions', 'S1', 'S2']
            # Test descriptions sheet content
            descriptions_sheet = wb['Descriptions']
            descriptions = {
                cell.value: descriptions_sheet.cell(row=row, column=2).value for
                row, cell in
                enumerate(descriptions_sheet['A'], start=1)}
            expected_descriptions = {None: 'Description',
                                     1: 'table_1_description',
                                     2: 'table_2_description'}
            assert descriptions == expected_descriptions
            # Test table content
            sheet_1 = wb['S1']
            sheet_2 = wb['S2']
            sheet_1_header = [cell.value for cell in sheet_1[1]]
            num_rows_s1 = sheet_1.max_row
            assert sheet_1_header == ['x1', 'x2', 'x3']
            assert num_rows_s1 == 11  # 10 rows + header
            sheet_2_header = [cell.value for cell in sheet_2[1]]
            num_rows_s2 = sheet_2.max_row
            assert sheet_2_header == ['x1', 'x2', 'x3', 'x4', 'x5', 'x6']
            assert num_rows_s2 == 16  # 15 rows + header
