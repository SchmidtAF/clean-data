'''
Pytest for utils.formatting.
'''
import pytest
from clean_data.utils import formatting


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestFormatEstimates(object):
    '''
    Testing `format_estimates`.
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @pytest.mark.parametrize(
        'point, se, lower, upper, alpha, round, exp, expected',
        [
        (0.2, 0.01, None, None, 0.05, 2, False, '0.20 (0.18; 0.22)'),
        (0.2, 0.01, None, None, 0.15, 2, False, '0.20 (0.19; 0.21)'),
        (0.2, 0.01, None, None, 0.05, 3, False, '0.200 (0.180; 0.220)'),
        (0.2, 0.01, None, None, 0.05, 2, True, '1.22 (1.20; 1.25)'),
        (0.2, None, 0.1, 0.35, 0.05, 2, False, '0.20 (0.10; 0.35)'),
        ]
    )
    def test_format_estimates(
        self, point, se, lower, upper, alpha, round, exp, expected
    ):
        results = formatting.format_estimates(
            point=point, se=se, lower=lower, upper=upper, alpha=alpha,
            round=round, exp=exp,
        )
        assert results == expected

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestSuperScriptinate(object):
    '''
    Testing `_superscriptinate`
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_superscriptinate(self):
        assert formatting._superscriptinate('12') == '¹²'
        assert formatting._superscriptinate('-204') == '⁻²⁰⁴'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestSubScriptinate(object):
    '''
    Testing `_subscriptinate`
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_subscriptinate(self):
        assert formatting._subscriptinate('12') == '₁₂'
        assert formatting._subscriptinate('+204.1') == '₊₂₀₄.₁'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestSciNotation(object):
    '''
    Testing `sci_notation`
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_sci_notation(self):
        assert formatting.sci_notation(1000) == '1.00×10³'
        assert formatting.sci_notation(2465640, sig_fig=4) == '2.4656×10⁶'
        assert formatting.sci_notation(4/2000, max=1/20) == '5.00×10⁻²'
        assert formatting.sci_notation(4/2000) == '2.00×10⁻³'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Testsuper_subs_notation(object):
    '''
    Testing `super_subs_notation`
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_super_subs_notation(self):
        text1 = 'this is a string with log_{10}(x), log_{2}{10}, and cm^{2} units'
        expt1 = 'this is a string with log₁₀(x), log₂{10}, and cm² units'
        text2 = 'this is a string without anything special'
        expt2 = text2
        assert formatting.super_subs_notation(text1) == expt1
        assert formatting.super_subs_notation(text2) == expt2

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestFormatNumerics(object):
    '''
    Testing functions for the `baseline` module
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_format_numerics(self):
        # string 1
        res1 = formatting.format_numerics(4, 3.334)
        assert res1 == '4.00 (3.33)'
        # string 2
        res2 = formatting.format_numerics(4, 3.334,
                                         round_within_brackets=1)
        assert res2 == '4.00 (3.3)'
        # string 3
        res3 = formatting.format_numerics(4, 3.334,
                                         round=3,
                                         round_within_brackets=1)
        assert res3 == '4.000 (3.3)'
        # string 4
        res4 = formatting.format_numerics(4, 3.334, 1.23,
                                         brackets=[' {', '>'],
                                         round=3,
                                         round_within_brackets=1)
        assert res4 == '4.000 {3.3; 1.2>'
        # string 5
        res5 = formatting.format_numerics(4, 3.334, 1.23,
                                         brackets=[' {', '>'],
                                         delimiter='+ ',
                                         round=4,
                                         round_within_brackets=2)
        assert res5 == '4.0000 {3.33+ 1.23>'

