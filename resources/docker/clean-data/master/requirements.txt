# Cython is need for statsmodels - will not be imported through pypi
# cython >= 0.29.33
# pandas>=1.6
# numpy>=1.18.5,<1.26
# scipy>=1.10.*
# statsmodels>=0.13.*
# pyarrow fails - is a future dependency of pandas, might be packed with that. 
# pyarrow
pytest>=6
pytest-mock>=3
pytest-dependency>=0.5
scikit-learn>=1
openpyxl==3.1
matplotlib>=3.4
bump2version>=1
jupyter
