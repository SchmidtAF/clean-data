clean_data.baseline
-------------------
.. automodule:: clean_data.baseline
   :members:

clean_data.format_tabular
-------------------------
.. automodule:: clean_data.format_tabular
   :members:

clean_data.prune
----------------
.. automodule:: clean_data.prune
   :members:

clean_data.time
---------------
.. automodule:: clean_data.time
   :members:

clean_data.missing
------------------
.. automodule:: clean_data.missing
   :members:

clean_data.utils.formatting
---------------------------
.. automodule:: clean_data.utils.formatting
   :members:

