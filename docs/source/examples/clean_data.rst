Modules
-------------------------------------------

Here is some sample templates showcasing 
the basic features of clean-data.

.. toctree::
   :maxdepth: 1

   modules/baseline.nblink
