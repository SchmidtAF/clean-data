.. clean-data documentation master file, created by
   sphinx-quickstart on Thu May 23 06:57:40 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to clean-data's documentation!
======================================

clean-data combines a set of python-based functions to clean and format tabular 
data for analysis or reports. For example, it can deal with making baseline tables, 
presenting numericals in scientific notation, and curating a dataframe for analysis
by removing whitespace, renaming column contents, order, as well as dealing dates/time 
data. 

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference
   
   api

.. toctree::
   :maxdepth: 3
   :caption: Examples
   
   examples

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing
   raising_issues



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
