#!/usr/bin/env python3
'''
A module to prune features
'''
from typing import Any, List, Type, Union, Tuple, Optional, Dict
from clean_data.errors import (
    is_type,
    is_df,
)
from clean_data.constants import(
    PruneNames as Pnames,
)
import pandas as pd
import numpy as np

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# identify collinear features
def pairwise_correlation(data:pd.DataFrame, threshold:float=0.90,
                         method:str='spearman', absolute:bool=True,
                         **kwargs:Optional[Any],
                         ) -> Tuple[pd.Series, pd.DataFrame]:
    '''
    Calculates pairwise correlation coefficients, and return pairs of features
    that have a correlation above the `threshold`. For negative thresholds it
    will look for smaller values, unless `absolute` is True.
    
    Parameters
    ----------
    data : pd.DataFrame
    threshold : float, default 0.90
        A float between -1 and 1.
    method : str, default `spearman`
        type of correlation coefficient, supplied to `pd.DataFrame.corr`.
    absolute : boo, default True
        do we need to take the absolute of the correlations before applying
        the threshold.
    **kwargs: any
        keyword arguments for `pd.DataFrame.corr`.
    
    Returns
    -------
    tuple : pd.Series, pd.DataFrame
        Returns a `pd.Series` with features that are pairwise correlated above
        the threshold. The pd.DataFrame contains the entire correlation matrix.
    
    '''
    
    # ### test input
    is_df(data)
    # is_type(data, pd.DataFrame)
    is_type(threshold, float)
    is_type(method, str)
    if (threshold < -1) | (threshold > 1):
        raise ValueError('`threshold` needs te be between -1, 1.')
    # ### do the actual work
    cor = data.corr(method=method)
    # sort values
    c1 = cor.stack().sort_values(ascending=False).drop_duplicates()
    high_cor = c1[c1.values!=1]
    # do we need to take the absolute
    if absolute == True:
        high_cor = np.abs(high_cor)
    if (absolute == False) & (threshold < 0):
        # below the threshold
        high = high_cor[high_cor<threshold]
    else:
        # above the threshold
        high = high_cor[high_cor>threshold]
    # reset index
    high = high.reset_index()
    high.index = high['level_0']; del high['level_0']
    high.index.name=Pnames.FEATURE_INDEX
    high.columns = [Pnames.FEATURE, Pnames.CORRELATION]
    # returns stuff
    return high, cor

