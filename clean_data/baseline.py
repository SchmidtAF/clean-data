from numbers import Real
from clean_data.errors import (
    is_type,
    are_columns_in_df,
    is_df,
    InputValidationError,
    _assign_empty_default,
    NotCalledError,
)
from clean_data.constants import (
    BaselineNames as BNames,
)
from clean_data.utils.formatting import (
    format_numerics,
    format_estimates,
    sci_notation,
)
from clean_data.utils.general import (
    combine_nested_dicts,
    likelihood_ratio_test,
)
from typing import (
    Callable, List, Union, Dict, Any, Literal, Self,
)
import sys
import copy
import inspect
import warnings
import numpy as np
import pandas as pd
import statsmodels.api as sm

'''
Module to estimate summary statistics and create (stratified) baseline tables.
'''

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# NOTE ADD to the error model - create an MSG class
MSG1 = 'In method `{}` for variable `{}`, test `{}` is skipping `{}`.'
MSG2 ='`{}` contains {} missing observations, these will be deleted.'
WARN1 = ('Removing missing values in strata. '
         'For tractability it is strongly adviced to '
         'address this problem before running the current, '
         'or future, analyses. For example by removing '
         'subjects with missing information on the '
         'stratifiction variable.')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BaselineTableStrata(object):
    '''
    Constructs a stratified table of summary statistics. Can apply optional
    tests/regression models to evaluate difference between strata. Currently
    this works for `scipy` and `statsmodels` methods.
    
    Parameters
    ----------
    data : `pd.DataFrame`
        A dataframe with `numerical` data.
    strata_name : `str`
        The column names in `data` to column stratify the table on.
    strata_max : `int` or `NoneType`, default 10
        Will raise a `ValueError` if there are more unique strata values. Set
        to `NoneType` to ignore.
    strata_sort : `bool`, default `True`
        Should the strata be sorted.
    continuous_variables : list
        A list of strings mapping to columns in data. Will summarize the column
        as mean (sd) and median (q1; q2).
    binary_variables : `list`
        A list of strings mapping to columns in data. Will summarize the column
        as sum (%) of one's.
    categorical_variables : `list`
        A list of strings mapping to columns in data. Will summarize the column
        as the frequency (%) of each unique values.
    verbose : `bool`, default `True`
        Whether warnings should be printed.
    
    Attributes
    ----------
    data : `pd.DataFrame`
        A copy of the input data.
    strata_list : `list`
        A list of unique strata values.
    copy : `bool`, default `False`
        Do we want to return a deep.copy of self.
    verbose : `bool`, default `True`
        Whether warnings should be printed.
    
    Methods
    -------
    test_difference(continuous_tests, binary_tests, categorical_tests, cov,
        missings, compact, strata_ref, intercept, pval_format, estimate_format)
    '''
    # /////////////////////////////////////////////////////////////////////////
    def __init__(self, data:pd.DataFrame,
                 strata_name:str, strata_max:int | None = 10,
                 strata_sort:bool | list[str] = True,
                 continuous_variables:None | list[str] = None,
                 binary_variables:None | list[str] = None,
                 categorical_variables:None | list[str] = None,
                 verbose:bool = True,
                 ) -> None:
        '''
        Initialises a `BaselineTableStrata` instance.
        
        '''
        # ### Checking input
        is_type(continuous_variables, (list, type(None)))
        is_type(categorical_variables, (list, type(None)))
        is_type(binary_variables,  (list, type(None)))
        is_type(strata_name, str)
        is_type(strata_max, (type(None), int, float))
        is_type(strata_sort, (list, bool))
        is_df(data)
        # ### get the unique number of strata
        strata_list = list(data[strata_name].unique())
        if not strata_max is None:
            if len(strata_list) > strata_max:
                raise ValueError(f'There are more than {strata_max} unique strata: '
                                 f'{len(strata_list)}.')
        if strata_sort == True:
            strata_list = list(np.sort(strata_list))
        elif strata_sort != False & isinstance(strata_sort, list):
            if set(strata_sort) == set(strata_list):
                strata_list = strata_sort
            else:
                raise ValueError("The content of `strata_sort` does not match "
                                 f"the strata levels:\n {strata_sort}, compared "
                                 f"to {strata_list}.")
        # cast to default types instead of numpy types
        strata_list = [l.item() if isinstance(l, np.generic) else l for l in\
                       strata_list]
        # check if there are nan is strata and remove if needed
        # first check for string nan
        if 'nan' in strata_list:
            if verbose == True:
                warnings.warn(WARN1)
            strata_list = [s  for s in strata_list if s != 'nan']
        # next make sure this is not a string and test for np.nan
        if not any(isinstance(s, str) for s in strata_list):
            if any(np.isnan(s) for s in strata_list):
                if verbose == True:
                    warnings.warn(WARN1)
                strata_list = [s  for s in strata_list if not np.isnan(s)]
        # ### assing things to self
        setattr(self, BNames.CLS_DATA, data)
        # NOTE will get updated __call__
        setattr(self, BNames.CLS_DATA_WO_MIS, data)
        setattr(self, BNames.CLS_CON_VARS, continuous_variables)
        setattr(self, BNames.CLS_BIN_VARS, binary_variables)
        setattr(self, BNames.CLS_CAT_VARS, categorical_variables)
        setattr(self, BNames.CLS_STRATA, strata_list)
        setattr(self, BNames.CLS_STRATA_NAME, strata_name)
        setattr(self, BNames.CLS_RETURN_COPY, True)
        setattr(self, BNames.CLS_VERBOSE, verbose)
        setattr(self, BNames.CLS_MISSINGS, BNames.CLS_LISTWISE)
        # confirm variables are in data
        vars = sum([
            v for v in [continuous_variables, binary_variables,
                        categorical_variables] if v is not None], [])
        are_columns_in_df(data, vars)
        setattr(self, BNames.CLS_ALL_VARS, vars + [strata_name])
    # /////////////////////////////////////////////////////////////////////////
    def __str__(self):
        CLASS_NAME = type(self).__name__
        return (f"{CLASS_NAME} instance with data=\n"
                f"{getattr(self, BNames.CLS_DATA).__str__()}, "
                f"continuous_variables={getattr(self, BNames.CLS_CON_VARS)}, "
                f"binary_variables={getattr(self, BNames.CLS_BIN_VARS)}, "
                f"categorical_variables={getattr(self, BNames.CLS_CAT_VARS)}, "
                f"strata_name={getattr(self, BNames.CLS_STRATA_NAME)}."
                )
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __repr__(self):
        CLASS_NAME = type(self).__name__
        return (f"{CLASS_NAME}(data=\n"
                f"{getattr(self, BNames.CLS_DATA).__str__()}, "
                f"continuous_variables={getattr(self, BNames.CLS_CON_VARS)}, "
                f"binary_variables={getattr(self, BNames.CLS_BIN_VARS)}, "
                f"categorical_variables={getattr(self, BNames.CLS_CAT_VARS)}, "
                f"strata_name={getattr(self, BNames.CLS_STRATA_NAME)})"
                )
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __call__(self,
                 missings:Literal['ignore', 'listwise', 'pairwise'] = 'ignore',
                 compact:bool=True,
                 round:int=2,
                 replace_na:str | Real | None = None,
                 ) -> Self:
        '''
        Creates a stratified baseline table.
        
        Parameters
        ----------
        missings : {`ignore`, `listwise`, `pairwise`}, default `ignore`
            How should missing outcome and/or covariates observations be
            addressed. Ignore will simply do nothing, listwise will subset
            these data on the participants who have information on all
            variables, pairwise will subset data on participants with
            complete observations for the variables used in a specific analysis.
            
            `ignore` will be mapped to dropna = False, and otherwise set to
            True in baseline_table calls.
        compact : `bool`, default `True`
            If the `N (%)` should be combined with the `Mean (SD)` into one column.
        round : `int`, default 2
            The desired rounding.
        replace_na : `str`, `int` or, `float`, default `NoneType`
            Replace NA's by this value.
        
        Attributes
        ----------
        table_bassline : `pd.DataFrame`
            The stratified baseline table.
        table_frequency : `pd.DataFrame`
            The number of non-missing values per variable (columns) stratified
            by stratum (index) values.
        
        Returns
        -------
        self : `BaselineTest` instance
            Returns the class instance with updated attributes.
        
        Notes
        -----
        Essentially subsets `data` on the unique entries of `strata_name` and
        applies `baseline_table` on each data-subset. The calls to
        baseline_table will internally check there is no overlap between
        the various variable types.
        '''
        # ### Checking input
        is_type(round, int)
        is_type(compact, bool)
        is_type(missings, str)
        setattr(self, 'replace_na', replace_na)
        data = getattr(self, BNames.CLS_DATA)
        if hasattr(self, BNames.CLS_BASELINE):
            delattr(self, BNames.CLS_BASELINE)
            delattr(self, BNames.CLS_BASELINE_CRUDE)
        # confirm missings
        self.MISSINGS = [BNames.CLS_LISTWISE, BNames.CLS_PAIRWISE,
                   BNames.CLS_IGNORE]
        if not missings in self.MISSINGS:
            raise InputValidationError(f'`missings` should equal '
                                       f'{self.MISSINGS}.')
        setattr(self, BNames.CLS_MISSINGS, missings)
        # ### do we remove rows listwise?
        data_miss =\
            data[
                data[getattr(self, BNames.CLS_ALL_VARS)].isnull().any(axis=1)
            ].index
        if len(data_miss) != 0:
            if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_LISTWISE:
                if getattr(self, BNames.CLS_VERBOSE) == True:
                    if len(data_miss) > 0:
                        warnings.warn(MSG2.format('data', len(data_miss)))
                data = data.loc[~data.index.isin(data_miss)]
        if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_PAIRWISE:
            dropna = True
        else:
            dropna = False
        # ### set frequency table
        setattr(self, BNames.CLS_FREQS, self._frequencies(data))
        # ### baseline per strata
        strata_name = getattr(self, BNames.CLS_STRATA_NAME)
        table_dict = {}
        for stratum in getattr(self, BNames.CLS_STRATA):
            # supress warnings if needed.
            if getattr(self, BNames.CLS_VERBOSE) == True:
                warnings.filterwarnings("ignore")
            # the actual baseline table
            table_dict[stratum] = baseline_table(
                data[data[strata_name] == stratum],
                continuous_variables = getattr(self, BNames.CLS_CON_VARS),
                binary_variables = getattr(self, BNames.CLS_BIN_VARS),
                categorical_variables = getattr(self, BNames.CLS_CAT_VARS),
                dropna=dropna,
                compact=compact,
                round=round,
                replace_na=getattr(self, BNames.CLS_REPLACE_NA),
            )
            # turn on warnings
            if getattr(self, BNames.CLS_VERBOSE) == True:
                warnings.resetwarnings()
        # #### final results table
        results = pd.concat(table_dict.values(), axis=1)
        # should be small enough to make the copy irrelevant.
        results_crude = results.copy()
        # adding multiIndex
        top_header = [
            s for s in  getattr(self, BNames.CLS_STRATA) for _ in\
            range(int(results.shape[1]/len(getattr(self, BNames.CLS_STRATA))))]
        results.columns = \
            pd.MultiIndex.from_arrays( [top_header, list(results.columns)])
        # ### removing some remaining nan
        if not getattr(self, BNames.CLS_REPLACE_NA) is None:
            results = self._replace_nan(results)
        # ### assing to self
        setattr(self, BNames.CLS_BASELINE, results)
        setattr(self, BNames.CLS_BASELINE_CRUDE, results_crude)
        # ### return self
        if getattr(self, BNames.CLS_RETURN_COPY) == True:
            return copy.deepcopy(self)
        else:
            return self
    # /////////////////////////////////////////////////////////////////////////
    def test_difference(
        self,
        continuous_tests: dict[str, dict] | None = None,
        binary_tests: dict[str, dict] | None = None,
        categorical_tests: dict[str, Callable] | None = None,
        cov:pd.DataFrame | None = None,
        missings:Literal['ignore', 'listwise', 'pairwise'] | None = None,
        compact:bool=False, intercept:bool=True,
        strata_ref:Real | str | None = None,
        pval_format: dict[str, dict] | None = None,
        estimate_format: dict[str, any] | None = None,
    ) -> pd.DataFrame:
        '''
        Updates the stratified baseline table with p-values for the difference
        between strata (e.g., studies/participant groups)
        Returns a stratified table of summary statistics per input variable.
        
        Parameters
        ----------
        continuous_tests : `dict` [`str`, `dict`] or `NoneType`, default `NoneType`
            The dictionary with string keys used to indicate the type of test
            and dictionary values. The inner dictionaries  should beformatted
            as in `constants.BaselineTests.__tests`, including a callable
            testing method, the p-value attribute, and an optional kwargs.
            This schema can be updated further depending on the downstream
            analysis, so please consult the `BaselineTests` data class.
        binary_tests : `dict` [`str`, `dict`] or `NoneType`, default `NoneType`
            The dictionary with string keys used to indicate the type of test
            and dictionary values. The inner dictionaries  should beformatted
            as in `constants.BaselineTests.__tests`, including an callable
            testing method, the p-value attribute, and an optional kwargs.
            This schema can be updated further depending on the downstream
            analysis, so please consult the `BaselineTests` data class.
        categorical_tests : `dict` [`str`, `dict`] or `NoneType`, default `NoneType`
            The dictionary with string keys used to indicate the type of test
            and dictionary values. The inner dictionaries  should beformatted
            as in `constants.BaselineTests.__tests`, including an callable
            testing method, the p-value attribute, and an optional kwargs.
            This schema can be updated further depending on the downstream
            analysis, so please consult the `BaselineTests` data class.
        cov : `pd.DataFrame`
            An optional table with the same number of rows as data containing
            covariates a regression model should be conditioned on.
        missings : {`ignore`, `listwise`, `pairwise`}, default `NoneType`
            
            Will overwrite the `missings` attribute unless set to `NoneType`.
            
            How should missing outcome and/or covariates observations be
            addressed. Ignore will simply do nothing, listwise will subset
            these data on the participants who have information on all
            variables, pairwise will subset data on participants with
            complete observations for the variables used in a specific analysis.
        compact : `bool`, default `False`
            Set to `True` to return results as two columns `Test` and
            `P-value`.
        strata_ref : `str`, `int`, `float` or `NoneType`, default `NoneType`
            Set to `NoneType` to treat the strata column as a numerical
            column in the regression (resulting in a trend relationship), this
            will automatically map string content to numerical data starting
            with 0 and apply a 1 step increase, until all the strings have been
            exhausted. Provide a non-NoneType value to create a k-1 dummy
            categorical matrix, where the indicated `strata_ref` will be removed
            through `pd.drop` - this will make this column the reference
            category.
        intercept : `bool`, default `True`
            Whether an intercept term should be included in a regression model.
        pval_format : `dict` [`str`, `dict`] or `NoneType`, default `NoneType`
            The p-value formatting to use. Set to `NoneType` to return the
            unformatted p-values. Set the outer dict key to `inequality` to
            return `< threshold` strings or set the key to `scientific` to
            use scientific notation. Any kwargs to `inq_notation` or
            `sci_notation` can be supplied in the inner dict.
        estimate_format : `dict` [`str`, `any`] or `NoneType`, default `NoneType`
            Set to an empty dict to apply `format_estimates` with default
            settings, or alternativly included kwargs in the dict. This will
            add to columns to the `table_regression_coef` with formatted
            point estimates and confidence intervals using their native scale
            as well as mapped through the `exponential function`.
        
        Attributes
        ----------
        table_baseline_test : `pd.DataFrame`
            A stratified baseline table with p-values results appended.
        table_regression_coef : `pd.DataFrame`
            A table with regression coefficients.
        
        Returns
        -------
        results: `pd.DataFrame`
            A baseline table with p-values for the difference.
        
        Notes
        -----
        The method has been tested against `scipy` and `statsmodels`, and should
        work for codebases which are similarly organised. Likely will need to
        be updated to work with bivariate time-to-event models such as
        implemented in `lifelines`.
        '''
        # ### Checking input
        is_type(continuous_tests, (dict, type(None)))
        is_type(categorical_tests, (dict, type(None)))
        is_type(binary_tests,  (dict, type(None)))
        is_type(cov,  (pd.DataFrame, type(None)))
        is_type(compact, bool)
        is_type(missings, (type(None), str))
        is_type(pval_format, (type(None), dict))
        is_type(estimate_format, (type(None), dict))
        data = getattr(self, BNames.CLS_DATA)
        strata_list = getattr(self, BNames.CLS_STRATA)
        # confirm there are tests defined
        test_none = all(x is None for x in
                        [continuous_tests, binary_tests, categorical_tests])
        if test_none:
            raise ValueError('Please provide some tests.')
        # confirm missing
        if not missings is None:
            if not missings in self.MISSINGS:
                raise InputValidationError(f'`missings` should equal '
                                           f'{self.MISSINGS}.')
            setattr(self, BNames.CLS_MISSINGS, missings)
        # confirm pval_dict only has one element
        if not pval_format is None:
            if len(pval_format) != 1:
                raise ValueError('`pval_format` should contain exactly one key.')
        # #### check if __call__ has been run
        if not hasattr(self, BNames.CLS_BASELINE):
            raise NotCalledError()
        # #### check missing data
        if not cov is None:
            cov_miss = cov.isnull().any(axis=1).sum()
            if cov_miss != 0:
                if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_LISTWISE:
                    if getattr(self, BNames.CLS_VERBOSE) == True:
                        warnings.warn(MSG2.format('cov', cov_miss))
                    cov = cov.dropna(axis=0)
        data_miss = data[
            data[getattr(self, BNames.CLS_ALL_VARS)].isnull().any(axis=1)
        ].index
        if len(data_miss) != 0:
            if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_LISTWISE:
                if getattr(self, BNames.CLS_VERBOSE) == True:
                    warnings.warn(MSG2.format('data', len(data_miss)))
                data = data.loc[~data.index.isin(data_miss)]
                # update self
                setattr(self, BNames.CLS_DATA_WO_MIS, data)
        # ### test for the differences per strata
        # Loop over variables if we want to have tests
        # NOTE NEED TO remove NAs from the var part
        c_test_results, b_test_results, cat_test_results = ({} for _ in range(3))
        c_reg_results, b_reg_results, cat_reg_results = ({} for _ in range(3))
        # ### try all continuous_tests
        if not continuous_tests is None:
            for t_name, c_dict in continuous_tests.items():
                test_results, reg_results = ({} for _ in range(2))
                for var in getattr(self,BNames.CLS_CON_VARS):
                    # first try regular tests, if this fails try regression
                    try:
                        # scipy tests
                        test_results[var] = self._strata_tests(
                            var_name=var, test_dict=c_dict, table=False,
                        )
                    except (ValueError, TypeError, ValueError):
                        # statsmodels regression
                        test_results[var], reg_results[var] =\
                            self._strata_regression(
                                var_name=var, test_dict=c_dict, cov=cov,
                                strata_ref=strata_ref, intercept=intercept,
                            )
                # store per test/reg
                c_test_results[t_name] = test_results
                c_reg_results[t_name] = reg_results
                del test_results, reg_results
        # ### try all binary_tests
        if not binary_tests is None:
            for t_name, b_dict in binary_tests.items():
                test_results, reg_results = ({} for _ in range(2))
                for var in getattr(self, BNames.CLS_BIN_VARS):
                    # first try regular tests, if this fails try regression
                    try:
                        # scipy tests
                        test_results[var] = self._strata_tests(
                            var_name=var, test_dict=b_dict, table=True,
                        )
                    except (ValueError, TypeError, ValueError):
                        # statsmodels regression
                        test_results[var], reg_results[var] =\
                            self._strata_regression(
                                var_name=var, test_dict=b_dict, cov=cov,
                                strata_ref=strata_ref, intercept=intercept,
                            )
                # store per test
                b_test_results[t_name] = test_results
                b_reg_results[t_name] = reg_results
                del test_results, reg_results
        # ### try all categorical test
        if not categorical_tests is None:
            for t_name, cat_dict in categorical_tests.items():
                test_results, reg_results = ({} for _ in range(2))
                for var in getattr(self, BNames.CLS_CAT_VARS):
                    # first try regular tests, if this fails try regression
                    try:
                        # scipy tests
                        test_results[var] = self._strata_tests(
                            var_name=var, test_dict=cat_dict, table=True,
                        )
                    except (ValueError, TypeError, ValueError):
                        # statsmodels regression
                        test_results[var], reg_results[var] =\
                            self._strata_regression(
                                var_name=var, test_dict=cat_dict,
                                cov=cov,
                                strata_ref=strata_ref, intercept=intercept,
                            )
                # store per test
                cat_test_results[t_name] = test_results
                cat_reg_results[t_name] = reg_results
                del test_results, reg_results
        # map to a single table
        # NOTE that the in __call__ used `baseline_table` function
        # already confirms there is no overlap in variables.
        pval_tab = pd.DataFrame(
            combine_nested_dicts(c_test_results,
                             combine_nested_dicts(
                                 b_test_results, cat_test_results)
                             )
        )
        setattr(self, '__pval_dict', pval_tab)
        # format p-values
        if not pval_format is None:
            if list(pval_format.keys())[0] == BNames.CLS_PVAL_FORM_SCI:
                pval_tab = pval_tab.map(lambda x: sci_notation(
                    x, **pval_format[BNames.CLS_PVAL_FORM_SCI]))
            if list(pval_format.keys())[0] == BNames.CLS_PVAL_FORM_INQ:
                pval_tab = pval_tab.map(lambda x: inq_notation(
                    x, **pval_format[BNames.CLS_PVAL_FORM_INQ]))
            # Making sure Nans persist
            pval_tab.fillna(np.nan)
        # #### compress p-value table
        if compact == True:
            compress_dict = {}
            if not getattr(self, BNames.CLS_REPLACE_NA) is None:
                pval_tab = self._replace_nan(pval_tab)
            for idx, row in pval_tab.iterrows():
                row_drop = row.dropna()
                compress_dict[idx] = {
                    BNames.TEST_PVALUE: '; '.join(
                        [str(v) for v in list(row_drop.values)]
                    ),
                    BNames.TEST_METHOD: '; '.join(row_drop.index),
                }
            pval_tab = pd.DataFrame.from_dict(compress_dict, orient='index')
        # #### regression results
        reg_table = pd.DataFrame()
        if len(c_reg_results) > 0:
            reg_table = type(self)._create_regression_table(c_reg_results)
        if len(b_reg_results) > 0:
            reg_table = pd.concat(
                [reg_table, type(self)._create_regression_table(b_reg_results)],
                axis=0)
        if len(cat_reg_results) > 0:
            reg_table = pd.concat(
                [reg_table, type(self)._create_regression_table(cat_reg_results)],
                axis=0)
        # format specific columns
        if reg_table.empty == False:
            if not pval_format is None:
                if list(pval_format.keys())[0] == BNames.CLS_PVAL_FORM_SCI:
                    reg_table[BNames.REG_PVALUE] = reg_table[BNames.REG_PVALUE].\
                        apply(lambda x: sci_notation(
                            x, **pval_format[BNames.CLS_PVAL_FORM_SCI]),
                              )
                if list(pval_format.keys())[0] == BNames.CLS_PVAL_FORM_INQ:
                    reg_table[BNames.REG_PVALUE] = reg_table[BNames.REG_PVALUE].\
                        apply(lambda x: inq_notation(
                            x, **pval_format[BNames.CLS_PVAL_FORM_INQ]),
                              )
                    # Making sure Nans persist
                    reg_table[BNames.REG_PVALUE].fillna(np.nan)
            if not estimate_format is None:
                # calculate lower and uppwer bound using the standard normal
                reg_table[BNames.REG_FORMAT_EST] = reg_table.apply(
                    lambda row: format_estimates(
                        point = row[BNames.REG_ESTIMATES],
                        se = row[BNames.REG_SE],
                        **estimate_format),
                    axis=1)
                # remove exp if needed
                estimate_format.pop('exp', None)
                reg_table[BNames.REG_FORMAT_EST2] = reg_table.apply(
                    lambda row: format_estimates(
                        point = row[BNames.REG_ESTIMATES],
                        se = row[BNames.REG_SE],
                        exp=True,
                        **estimate_format),
                    axis=1)
        # assign to self
        setattr(self, BNames.CLS_REGRESSION, reg_table)
        # #### final results table
        results = getattr(self, BNames.CLS_BASELINE_CRUDE)
        top_header = [s for s in strata_list for _ in\
                      range(int(results.shape[1]/len(strata_list)))]
        # #### format p-values
        if pval_tab.empty == False:
            # add the first categorical index
            new_index = []
            for idx in pval_tab.index:
                if idx in getattr(self, BNames.CLS_ALL_VARS):
                    new_index.append(
                        results.index[results.index.str.contains(
                            idx, regex=False)][0])
                else:
                    new_index.append(idx)
            # assing index to pval_tab
            pval_tab.index = new_index
            # merge
            results = results.merge(
                pval_tab, left_index = True, right_index=True, how='left')
            # update top_header
            top_header =\
                top_header + ['Difference' for _ in range(pval_tab.shape[1])]
        # #### add MultiIndex header
        results.columns = \
            pd.MultiIndex.from_arrays( [top_header, list(results.columns)])
        # ### removing some remaining nan
        if not getattr(self, BNames.CLS_REPLACE_NA) is None:
            results = self._replace_nan(results)
        # #### assign to self
        setattr(self, BNames.CLS_BASELINE_DIF, results)
        # #### return stuf
        return getattr(self, BNames.CLS_BASELINE_DIF)
    # /////////////////////////////////////////////////////////////////////////
    def _replace_nan(self, data):
        """Replaces nan values by an user supplied alternative value"""
        data=data.fillna(getattr(self, BNames.CLS_REPLACE_NA))
        data=data.replace('nan (nan)',
                                getattr(self, BNames.CLS_REPLACE_NA))
        data=data.replace('nan (nan; nan)',
                                getattr(self, BNames.CLS_REPLACE_NA))
        data=data.replace('nan',
                                getattr(self, BNames.CLS_REPLACE_NA))
        return data

    # /////////////////////////////////////////////////////////////////////////
    def _frequencies(self, data:pd.DataFrame) -> pd.DataFrame:
        '''
        Creates a frequency table counting the number of non-missing values by
        stratum level.
        
        Returns
        -------
        counts : `pd.DataFrame`
            A frequency table with the strata values on the index and the
            variable names on the columns.
        '''
        # #### check input
        is_df(data)
        # Get frequncies
        count_dict = {}
        strata_name = getattr(self, BNames.CLS_STRATA_NAME)
        for c in data.columns:
            # skip strat itself
            if c != strata_name:
                count_dict[c] =\
                    data.groupby(strata_name)[c].apply(
                        lambda x: x.notna().sum()).to_dict()
        counts = pd.DataFrame(count_dict)
        # sort by index
        counts = counts.sort_index()
        # return
        return counts
    # /////////////////////////////////////////////////////////////////////////
    def _del_rows(self, var_name:str) -> pd.DataFrame:
        '''
        An internal fucntion to remove rows based on a combination of
        `strata_name` and `var_name`. Will perform a pairwise deletion of rows
        without information on both `strata_name` and `var_name`.
        
        Parameters
        ----------
        var_name : `str`
            The column name for the outcome variable.
        
        Returns
        -------
        data : `pd.DataFrame`
        '''
        # copying data
        input_data = getattr(self, BNames.CLS_DATA_WO_MIS).loc[:,
            [getattr(self, BNames.CLS_STRATA_NAME), var_name]].copy()
        # getting missing observations
        if getattr(self, BNames.CLS_VERBOSE) == True:
            missing_rows = input_data.isnull().any(axis=1).sum()
            if missing_rows > 0:
                warnings.warn(MSG2.format(var_name, missing_rows))
        # drop missing data
        input_data = input_data.dropna(axis=0)
        # return
        return input_data
    # /////////////////////////////////////////////////////////////////////////
    def _strata_tests(self, var_name:str,
                      test_dict:dict[str, Any], table:bool=False,
                      return_instance:bool=False,
                      ) -> float | Any:
        '''
        An internal function to apply a test on data and return a p-value, or
        optionally return the entire test instance.
        
        Parameters
        ----------
        var_name : `str`
            The column name for the outcome variable.
        test_dict : `dict` [`str`, `any`]
            A dictionary containing the keys `Method`, `P-value`, and `kwargs`
            with matching values {`Callable`, `str` or `int`, `dict`}.
        table : `bool`, default `False`
            Whether a table should be created comparing the strata and outcome
            variable frequencies.
        return_instance : `bool`, default `False`
            Whether the instance should be returned instead of the p-value
            float.  This is potentially useful for debugging.
        
        Returns
        -------
        results : `float` or `class instance`
            Either returns the p-value or the class instance of the applied
            test.
        '''
        # #### check input
        is_type(test_dict, dict)
        is_type(return_instance, bool)
        input_data = getattr(self, BNames.CLS_DATA_WO_MIS)
        # #### remove missing data, pairwise
        if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_PAIRWISE:
            input_data = self._del_rows(var_name=var_name)
        data_miss =input_data.isnull().any(axis=1).sum()
        # #### run analysis
        if table == False:
            data = [list(
                input_data.loc[
                    input_data[getattr(self, BNames.CLS_STRATA_NAME)] == s,
                    var_name]
            ) for s in getattr(self, BNames.CLS_STRATA)
                    ]
            # run the tests
            try:
                res = test_dict[BNames.TEST_METHOD](
                    *data, **test_dict[BNames.TEST_KWARGS],
                                                    )
            except (KeyError, TypeError):
                    res = test_dict[BNames.TEST_METHOD](*data)
        else:
            data = pd.crosstab(
                input_data[getattr(self, BNames.CLS_STRATA_NAME)],
                input_data[var_name]
            )
            # run the tests
            try:
                res = test_dict[BNames.TEST_METHOD](
                    data, **test_dict[BNames.TEST_KWARGS],
                                                    )
            except (KeyError, TypeError):
                    if getattr(self, BNames.CLS_VERBOSE) == True:
                        warnings.warn(MSG1.format(
                            inspect.currentframe().f_code.co_name,
                            var_name,
                            test_dict[BNames.TEST_METHOD],
                            BNames.TEST_KWARGS),
                                      )
                    res = test_dict[BNames.TEST_METHOD](data)
        if return_instance == False:
            # get p-values
            try:
                res = res[test_dict[BNames.TEST_PVALUE]]
            except TypeError:
                res = getattr(res, test_dict[BNames.TEST_PVALUE])
        # removing possibly numpy types
        if isinstance(res, float):
            try:
                res = res.item()
            except AttributeError:
                pass
        # return
        return res
    # /////////////////////////////////////////////////////////////////////////
    def _strata_regression(self,
                           var_name:str,
                           test_dict: dict[str, Any],
                           cov:pd.DataFrame | None = None,
                           cov_confirm_indices:bool = False,
                           strata_ref:Real | str | None = None,
                           intercept:bool=True,
                           ) -> tuple[float, dict[str, dict]]:
        '''
        An internal function to apply a run regression analysis comparing the
        strata_name column to the var_name column.
        
        Parameters
        ----------
        var_name : `str`
            The column name for the outcome variable.
        test_dict : `dict` [`str`, `any`]
            A dictionary containing the keys `Method`, `P-value`, and `kwargs`
             with matching values {`Callable`, `str` or `int`, `dict`}.
        cov : `pd.DataFrame`
            An optional table with the same number of rows as data containing
            covariates the regression model should be conditioned on.
        cov_confirm_indices : `bool`, default `False`
            Whether to confirm the `data` and `cov` row indices are the same
            and in identical order.
        strata_ref : `str`, `int`, `float` or `NoneType`, default `NoneType`
            Set to `NoneType` to treat the strata column as a numerical
            column in the regression (resulting in a trend relationship), this
            will automatically map string content to numerical data starting
            with 0 and apply a 1 step increase, until all the strings have been
            exhausted. Provide a non-NoneType value to create a k-1 dummy
            categorical matrix, where the indicated `strata_ref` will be
            removed through `pd.drop` - this will make this column the
            reference category.
        intercept : `bool`, default `True`
            Whether an intercept term should be included in the regression
            model.
        return_instance : `bool`, default `False`
            Whether the instance should be returned instead of the p-value
            float.  This is potentially useful for debugging.
        
        Returns
        -------
        results : `tuple` [`float`, `dict`]
            Return the overal p-value and a dictionary with point estimates,
            standard esimates, and p-values.
        '''
        # ### Checking input
        is_type(var_name, str)
        is_type(cov, (type(None), pd.DataFrame))
        is_type(cov_confirm_indices, bool)
        is_type(strata_ref, (type(None), str, Real))
        is_type(intercept, bool)
        data = getattr(self, BNames.CLS_DATA_WO_MIS)
        # #### remove missing data, pairwise
        if getattr(self, BNames.CLS_MISSINGS) == BNames.CLS_PAIRWISE:
            data = self._del_rows(var_name=var_name)
        # ### getting outcome and exposures
        Y = data[var_name].to_numpy()
        Y = Y.reshape(Y.shape[0], 1)
        # ### do we need x to be categorical
        if not strata_ref is None:
            X = pd.get_dummies(data[getattr(self, BNames.CLS_STRATA_NAME)],
                               drop_first=False, dtype=int)
            # try/except in case the types do not match up.
            try:
                X = X.drop([strata_ref], axis=1).to_numpy()
            except KeyError as KE:
                print('Types:', file=sys.stderr)
                print(type(strata_ref), file=sys.stderr)
                print(X.columns.dtype, file=sys.stderr)
                print('Content:', file=sys.stderr)
                print(strata_ref, file=sys.stderr)
                print(X.columns, file=sys.stderr)
                raise KE
            
        else:
            X = data[getattr(self, BNames.CLS_STRATA_NAME)].to_numpy()
        # ### if not categorical check if X is string and convert to numeric
        if strata_ref is None:
            if data[getattr(self, BNames.CLS_STRATA_NAME)].apply(
                    lambda x: isinstance(x, str)).any():
                X_uniq = np.sort(
                    data[getattr(self, BNames.CLS_STRATA_NAME)].unique())
                X_dict = {s: i for i, s in enumerate(set(X_uniq))}
                X = np.array([X_dict[s] for s in\
                              data[getattr(self, BNames.CLS_STRATA_NAME)]])
        # making sure there is a 2D matrix
        if len(X.shape) == 1:
            X = X.reshape(X.shape[0], 1)
        # variable names
        variable_names = [getattr(self, BNames.CLS_STRATA_NAME)]
        if X.shape[1] > 1:
            # NOTE the [1:] is needed because of the dummy coding
            sort_val = data[getattr(self, BNames.CLS_STRATA_NAME)].\
                            dropna().sort_values().unique()
            variable_names =\
                [variable_names[0]  + BNames.CAT_DELIM + str(s) for\
                 s in sort_val if s != strata_ref]
        # ### check if covariates need to be added
        if not cov is None:
            if cov_confirm_indices == True:
                if data.index.equals(cov.index) == False:
                    # get the indices which do not overlap or are out of order
                    violations = data.index.difference(cov.index).tolist() +\
                        cov.index.difference(data.index).tolist() +\
                    [v for v in data.index.intersection(cov.index) if\
                     data.index.get_loc(v) != cov.index.get_loc(v)]
                    # raise error
                    raise IndexError("The cov indices do not match the indices "
                                     "for data in terms of overlap and order. "
                                     "These are the violating indices: "
                                     f"{violations}.")
            if X.shape[0] != cov.shape[0]:
                raise IndexError('exposure and covariate matrices have a different '
                                 f'number of rows: {X.shape[0]}, {cov.shape[0]}.'
                                 )
            # column bind
            X = np.column_stack((X, cov.to_numpy()))
            # add to variable list
            variable_names = variable_names + cov.columns.to_list()
            # NOTE not sure why the below is here ..., leaving it for the moment...
            # if not strata_ref is None:
            #     X_base = cov.to_numpy()
        # ### do we need an intercept
        if intercept == True:
            X = sm.add_constant(X)
            # see if we need to do the same to the baseline model without X
            try:
                X_base = sm.add_constant(X_base)
            except NameError:
                X_base = np.ones((X.shape[0], 1))
            # add intercept
            variable_names = [BNames.REG_INT] + variable_names
        # ### ensure Y and X do not include nested numpy arrays
        # NOTE currently mapping everything to `float`,
        # shold explore how to check whether specific columns or
        # entire arrays are better off encoded as int.
        if isinstance(Y, object):
            Y = np.concatenate([arr for arr in Y]).astype(float)
        if isinstance(X, object):
            org_shape = X.shape
            X = np.concatenate([arr for arr in X]).astype(float)
            X = X.reshape(org_shape)
        # ### fit the model
        try:
            model_temp = test_dict[BNames.TEST_METHOD](
                endog=Y, exog=X, **test_dict[BNames.TEST_KWARGS])
            model = model_temp.fit()
        except (KeyError, TypeError):
            if getattr(self, BNames.CLS_VERBOSE) == True:
                warnings.warn(MSG1.format(
                    inspect.currentframe().f_code.co_name,
                    var_name,
                    test_dict[BNames.TEST_METHOD],
                    BNames.TEST_KWARGS),
                              )
            model_temp = test_dict[BNames.TEST_METHOD](endog=Y,exog=X,)
            model = model_temp.fit()
        # ### get overall p-value
        pval_temp = pd.DataFrame(getattr(model, BNames.SM_ATTR_PVALUE),
                                 index=variable_names)
        if not strata_ref is None:
            # In this case we need to compare the full model to the baseline
            # model without the exposure to get the overall p-value
            model_base = test_dict[BNames.TEST_METHOD](Y,X_base).fit()
            try:
                _, pvalue_overall, _, =  model.compare_lr_test(model_base)
                pvalue_overall = float(pvalue_overall)
            except AttributeError:
                
                _, pvalue_overall = likelihood_ratio_test(
                    ll_full=model.llf, ll_sub=model_base.llf,
                    df = len(model.params) - len(model_base.params))
        else:
            pvalue_overall = pval_temp.iloc[test_dict[BNames.TEST_PVALUE]]
        # ### get estimates
        est_temp = pd.DataFrame(getattr(model, BNames.SM_ATTR_PARAMS),
                                index=variable_names)
        # ### get standard errors
        se_temp = pd.DataFrame(getattr(model, BNames.SM_ATTR_SE),
                               index=variable_names)
        # getting cases and total sample size
        cases = Y.sum().item()
        samples = X.shape[0]
        if not cases.is_integer():
            cases = 0
        # ### returning stuff
        try:
            if len(pvalue_overall) == 1:
                pvalue_overall = float(pvalue_overall.iloc[0])
        except TypeError:
            pass
        # return
        return pvalue_overall, {
            BNames.REG_ESTIMATES : est_temp,
            BNames.REG_SE        : se_temp,
            BNames.REG_PVALUE    : pval_temp,
            BNames.REG_MODEL     : model,
            BNames.REG_CASES     : cases,
            BNames.REG_SAMPLES   : samples,
        }
    # /////////////////////////////////////////////////////////////////////////
    @staticmethod
    def _create_regression_table(results: dict[str, dict[str, dict]],
                                 ) -> pd.DataFrame:
        '''
        Creeates a table with regression results.
        
        Will also include formatting steps here - I think.
        
        Parameters
        ----------
        results : `dict` [`str`, `dict`]
            A nested dictionary with the outer dict collecting various
            `models`, the second dictionary the `outcomes`, and the inner
            dictionaries `point estimates`, `standard errors`, and `p-values`.
        
        Returns
        -------
        table : `pd.DataFrame`
            A long-formatted table with regression coefficient results.
        '''
        # ### Checking input
        is_type(results, dict)
        # ### collect data
        table_rows = []
        # Loop over the dictionary results
        for m_name, m_results in results.items():
            for v_name, v_results in m_results.items():
                point_estimates = v_results[BNames.REG_ESTIMATES]
                standard_errors = v_results[BNames.REG_SE]
                p_values = v_results[BNames.REG_PVALUE]
                cases = v_results[BNames.REG_CASES]
                samples = v_results[BNames.REG_SAMPLES]
                # Add dictionary to list
                for index in point_estimates.index:
                    table_rows.append({
                        BNames.REG_VAR       : v_name,
                        BNames.REG_MODEL     : m_name,
                        BNames.REG_EXPOS     : index,
                        BNames.REG_CASES     : cases,
                        BNames.REG_SAMPLES   : samples,
                        BNames.REG_ESTIMATES : float(point_estimates.loc[index].values[0]),
                        BNames.REG_SE        : float(standard_errors.loc[index].values[0]),
                        BNames.REG_PVALUE    : float(p_values.loc[index].values[0]),
                    })
        # finale table
        table = pd.DataFrame(table_rows)
        # update index
        if table.empty == False:
            table.index = table[BNames.REG_MODEL]
            del table[BNames.REG_MODEL]
        # ### return
        return table

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def baseline_table(data:pd.DataFrame,
                   continuous_variables:Union[None, List[str]]=None,
                   binary_variables:Union[None, List[str]]=None,
                   categorical_variables:Union[None, List[str]]=None,
                   dropna:bool=False,
                   compact:bool=True,
                   round:int=2,
                   replace_na:Union[str, int, float, None] = None,
                   ) -> pd.DataFrame:
    '''
    Returns a table of summary statistics per input variable.
    
    Parameters
    ----------
    data : `pd.DataFrame`
        A dataframe with `numerical` data.
    continuous_variables : list
        A list of strings mapping to columns in data. Will summarize the column
        as mean (sd) and median (q1; q2).
    binary_variables : `list`
        A list of strings mapping to columns in data. Will summarize the column
        as sum (%) of 1.
    categorical_variables : `list`
        A list of strings mapping to columns in data. Will summarize the column
        as the frequency (%) of each unique values.
        
        `Note` this should be able to process string columns.
    dropna : `bool`, default `False`
        Whether missing values should be removed before estimating the
        summary statistics.
    compact : `bool`, default `True`
        If the `N (%)` should be combined with the `Mean (SD)` into one column.
    round : `int`, default 2
        The desired rounding.
    replace_na : `str`, `int` or, `float`, default `NoneType`
        Replace NA's by this value.
    
    Returns
    -------
    table: `pd.DataFrame`
        A baseline table.
    
    Notes
    -----
    The table will be formatted as:
    
    | Variable             | N (%) | Mean (SD) | Median (Q1; Q3) | Missing (%) |
    | Total no. of samples | ...   | ...       | ...             | ...         |
    
    or as:
    
    | Variable             | Mean (SD) or N (%) | Median (Q1; Q3) | Missing (%) |
    | Total no. of samples | ...                | ...             | ...         |
    
    with SD for the sample standard deviation, N for the number of subject
    with a certain characteristic, Q for quantile, and % the percentage of the
    overall sample size.
    '''
    # ### Checking input
    is_type(continuous_variables, (list, type(None)), 'continuous_variables')
    is_type(categorical_variables, (list, type(None)), 'categorical_variables')
    is_type(binary_variables,  (list, type(None)), 'binary_variables')
    is_type(round, int)
    is_type(dropna, bool)
    is_type(compact, bool)
    # set None to empty list
    continuous_variables, categorical_variables, binary_variables =\
        _assign_empty_default(
            [continuous_variables, categorical_variables, binary_variables],
            empty_object=list,
    )
    # set variables
    vars = continuous_variables + binary_variables + categorical_variables
    is_df(data)
    are_columns_in_df(data, vars)
    # check if unique columns
    if len(np.unique(vars)) != len(vars):
        raise InputValidationError('The variables arguments contain '
                                   'duplicates.')
    # check if binary is really binary.
    bin_check =\
        [s for s in binary_variables if  data[s].isin([0,1, np.nan]).all() ==
         False ]
    if len(bin_check) > 0:
        raise ValueError('The following variables containts values other than '
                         '`0` and `1`:',
                         bin_check,
                         )
    # ### local constants
    MEAN = BNames.MEAN; SD = BNames.SD; N=BNames.N;
    MISSING=BNames.MISSING; MEDIAN = BNames.MEDIAN
    Q1=BNames.Q1; Q3=BNames.Q3
    COUNT = BNames.COUNT; PROP=BNames.PROP
    TABLE = BNames.TABLE
    TOTAL_SAMPLE=BNames.TABLE_TOTAL_SAMPLE
    COL1 = BNames.TABLE_COL1
    COL2 = BNames.TABLE_COL2
    COL3 = BNames.TABLE_COL3
    COL4 = BNames.TABLE_COL4
    COL5 = BNames.TABLE_COL5
    if compact == False:
        COLUMNS = [COL2, COL3, COL4, COL5]
    else:
        COL2 = COL2 + BNames.OR_DELIM + COL3
        COL3 = COL2
        COLUMNS = [COL2, COL4, COL5]
    # ### doing the work
    cat_names = []
    for s in categorical_variables:
        sort_val = data[s].dropna().sort_values().unique()
        cat_names = cat_names +\
            [str(s) + BNames.CAT_DELIM + str(i) for i in sort_val]
    # empty data frame
    table = pd.DataFrame(index=[TOTAL_SAMPLE]+continuous_variables+binary_variables+cat_names,
                         columns=COLUMNS)
    table.index.name = COL1
    # get Summary Instance
    TabInst = ColumnSummary(data)
    for var in continuous_variables:
        mean = TabInst.symmetrical(var, dropna)
        median = TabInst.skewed(var, dropna)
        # add the mean
        table.loc[var, COL3] = format_numerics(float(mean[MEAN]),
                                                float(mean[SD]),
                                                round=round,
                                                )
        # add the median
        table.loc[var, COL4] = format_numerics(float(median[MEDIAN]),
                                                float(median[Q1]),
                                                float(median[Q3]),
                                                round=round,
                                                )
        # percentage missing
        try:
            perc_m = median[MISSING]/median[N]*100
        except ZeroDivisionError:
            perc_m = 0
        table.loc[var, COL5] = format_numerics(float(median[MISSING]),
                                                float(perc_m),
                                                round=0,
                                                round_within_brackets=round,
                                                )
    for var in binary_variables:
        prop = TabInst.binary(var, dropna)
        # add the counts and proportions
        table.loc[var, COL2] = format_numerics(float(prop[COUNT]),
                                                float(prop[PROP]*100),
                                                round=0,
                                                round_within_brackets=round,
                                                )
        # percentage missing
        try:
            perc_m = prop[MISSING]/prop[N]*100
        except ZeroDivisionError:
            perc_m = 0
        table.loc[var, COL5] = format_numerics(float(prop[MISSING]),
                                                float(perc_m),
                                                round=0,
                                                round_within_brackets=round,
                                                )
    for var in categorical_variables:
        cat = TabInst.categorical(var, dropna)
        # adding table
        table_cat = cat[TABLE]
        # remove rows with an 'nan' index
        index_list = [idx for idx in table_cat.index if not idx == 'nan']
        table_cat = table_cat.loc[index_list]
        table_cat.index = [var + BNames.CAT_DELIM + str(s) \
                                       for s in table_cat.index
                           ]
        # adding to table
        table_cat['string'] = [
            format_numerics(float(table_cat.loc[s, COUNT]),
                             float(table_cat.loc[s, PROP] * 100),
                             round=0,
                             round_within_brackets=round,
                             ) for s in table_cat.index
        ]
        table.loc[table_cat.index, COL2] = table_cat['string']
        # add missing data
        try:
            perc_m = cat[MISSING]/cat[N]*100
        except ZeroDivisionError:
            perc_m = 0
        table.loc[table_cat.index, COL5] =  \
            format_numerics(float(cat[MISSING]),
                             float(perc_m),
                             round=0,
                             round_within_brackets=round,
                             )
    # replace NA?
    if not replace_na is None:
        table=table.fillna(replace_na)
        table=table.replace('nan (nan; nan)', replace_na)
    # add total sample size
    if compact == False:
        table.loc[TOTAL_SAMPLE,COL2] = data.shape[0]
    else:
        table.loc[TOTAL_SAMPLE,COL3] = data.shape[0]
    # return
    return table

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ColumnSummary(object):
    '''
    A class to estimate summary statistics of a column in `data`.
    
    Parameters
    ----------
    data: `pd.DataFrame`
        A DataFrame which needs to be summarized.
    
    Attributes
    ----------
    data: pd.DataFrame
        The supplied DataFrame.
    sample_size: int
        The number of rows in `data`.
    
    Methods
    -------
    get_missing(column_list)
        Returns the number of NaN's a list.
    symmetrical(column, dropna=False)
        Calculates the mean and standard deviation.
    skewed(column, dropna=False)
        Calculates the median, and the first and third quantile.
    binary(column, dropna=False)
        Calculates the sum and the mean.
    categorical(column, dropna=False)
        Calculates the frequency and proportion of each unique value.
    '''
    # /////////////////////////////////////////////////////////////////////////
    def __init__(self, data:pd.DataFrame):
        '''
        Assign column to self
        
        '''
        self.data=data.copy()
        self.sample_size=self.data.shape[0]
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __str__(self):
        CLASS_NAME = type(self).__name__
        return (f"{CLASS_NAME} instance with data=\n{self.data.__str__()}. "
                )
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __repr__(self):
        CLASS_NAME = type(self).__name__
        return f"{CLASS_NAME}(data=\n{self.data.__repr__()})"
    # /////////////////////////////////////////////////////////////////////////
    @staticmethod
    def get_missing(column_list:list) -> int:
        '''
        Returns the number of missing observations.
        
        Parameters
        ----------
        column_list: `list`
            A list of element from `data`.
        
        Returns
        -------
        missing: `int`
            The number of missing observations.
        '''
        try:
            # if numeric
            missing=np.count_nonzero(np.isnan(column_list))
        except TypeError:
            # if string
            missing=len([i for i in column_list if i is np.nan])
        # return
        return missing
    # /////////////////////////////////////////////////////////////////////////
    def symmetrical(self, column:str, dropna:bool=False,
                    ) -> Dict[str, Union[int, float]]:
        '''
        Estimates the mean and standard deviation of `column`, returns a
        dictionary.
        
        Parameters
        ----------
        column: `str`
            Column name in `data`.
        dropna: `bool`, default `False`
            Whether missing values should be removed before estimating the
            summary statistics.
        
        Returns
        -------
        results: `dict`
            A dictionary with summary statistics.
        '''
        # subset data
        column_list = self.data[column].to_list()
        # do the calculations
        self.missing = type(self).get_missing(column_list)
        if dropna == False:
            if len(column_list) == 0:
                dict_results = {BNames.MEAN: np.nan,
                                BNames.SD: np.nan,
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
            else:
                dict_results = {BNames.MEAN: np.mean(column_list),
                                BNames.SD: np.std(column_list),
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
        elif dropna == True:
            dict_results = {BNames.MEAN: np.nanmean(column_list),
                            BNames.SD: np.nanstd(column_list),
                            BNames.N: self.sample_size,
                            BNames.MISSING: self.missing,
                            }
        return dict_results
    # /////////////////////////////////////////////////////////////////////////
    def skewed(self, column:str, dropna:bool=False,
               ) -> Dict[str, Union[int, float]]:
        '''
        Estimates the median, quartile 1 and 3 of `column`, returns a
        dictionary.
        
        Parameters
        ----------
        column: `str`
            Column name in `data`.
        dropna: `bool`, default `False`
            Whether missing values should be removed before estimating the
            summary statistics.
        
        Returns
        -------
        results: `dict`
            A dictionary with summary statistics.
        '''
        # subset data
        column_list = self.data[column].to_list()
        # do the calculations
        self.missing = type(self).get_missing(column_list)
        if dropna == False:
            if len(column_list) == 0:
                dict_results = {BNames.MEDIAN: np.nan,
                                BNames.Q1: np.nan,
                                BNames.Q3: np.nan,
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
            else:
                dict_results = {BNames.MEDIAN: np.median(column_list),
                                BNames.Q1: np.quantile(column_list, q=0.25),
                                BNames.Q3: np.quantile(column_list, q=0.75),
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
        elif dropna == True:
            dict_results = {BNames.MEDIAN: np.nanmedian(column_list),
                            BNames.Q1: np.nanquantile(column_list, q=0.25),
                            BNames.Q3: np.nanquantile(column_list, q=0.75),
                            BNames.N: self.sample_size,
                            BNames.MISSING: self.missing,
                            }
        return dict_results
    # /////////////////////////////////////////////////////////////////////////
    def binary(self, column:str, dropna:bool=False,
               ) -> Dict[str, Union[int, float]]:
        '''
        Counts (sum) the number of 1 entries in `column` as well as its
        poportion (mean).
        
        Parameters
        ----------
        column: `str`
            Column name in `data`.
        dropna: `bool`, default `False`
            Whether missing values should be removed before estimating the
            summary statistics.
        
        Returns
        -------
        results: `dict`
            A dictionary with summary statistics.
        '''
        # subset data
        column_list = self.data[column].to_list()
        # do the calculations
        self.missing = type(self).get_missing(column_list)
        if dropna == False:
            if len(column_list) == 0:
                dict_results = {BNames.COUNT: 0,
                                BNames.PROP: 0,
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
            else:
                dict_results = {BNames.COUNT: np.sum(column_list),
                                BNames.PROP: np.mean(column_list),
                                BNames.N: self.sample_size,
                                BNames.MISSING: self.missing,
                                }
        elif dropna == True:
            dict_results = {BNames.COUNT: np.nansum(column_list),
                            BNames.PROP: np.nanmean(column_list),
                            BNames.N: self.sample_size,
                            BNames.MISSING: self.missing,
                            }
        return dict_results
    # /////////////////////////////////////////////////////////////////////////
    def categorical(self, column:str, dropna=False,
                    ) -> Dict[str, Union[int, float]]:
        '''
        Estimates the frequency of each unique value, along with its proportion.
        
        Parameters
        ----------
        column: `str`
            Column name in `data`.
        dropna: `bool`, default `False`
            Whether missing values should be removed before estimating the
            summary statistics.
        
        Returns
        -------
        results: `dict`
            A dictionary with summary statistics.
        '''
        # subset data
        column_list = self.data[column].to_list()
        # do the calculations
        self.missing = type(self).get_missing(column_list)
        value, freq = np.unique(column_list, return_counts=True)
        if dropna == False:
            denom=self.sample_size
        elif dropna == True:
            denom=self.sample_size-self.missing
        # calculate table
        table = pd.DataFrame({BNames.COUNT: freq,
                              BNames.PROP: freq/denom,
                              },
                             index=value)
        # removing index with missing data
        table = table[table.index.notna()]
        # results dict
        dict_results = {BNames.TABLE: table,
                        BNames.N: self.sample_size,
                        BNames.MISSING: self.missing,
                        }
        return dict_results


