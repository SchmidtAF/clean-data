'''
Miscellaneous functions to format tabular data. Most function are focussed on
Creating publication column content (e.g., labels), or processing data from
data types (e.g., from BNF/ChEMBL look-up for target druggability)
'''
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# import
import pandas as pd
import numpy as np
from types import FunctionType
from typing import Any, List, Type, Union, Tuple, Dict, Optional
from clean_data.errors import (
    is_type,
    is_df,
    are_columns_in_df,
    InputValidationError,
    _assign_empty_default,
)
from clean_data.constants import (
    FormatTabular as TableNames,
)

# constants
MAXLOG10 = 20

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def nlog10_func(values:List[float], max_log10:float=MAXLOG10) -> List[float]:
    """
    computes the negative log10 value of a list of floats.
    
    Parameters
    ----------
    val : pandas.Series
        A list of floats
    max_log10 : float, default 20
        Cutoff which replaces floats which were zero in `values`, and
        truncates larger values.
    
    Returns
    -------
    results: list of floats
        A list of negative log10 transformed floats.
    """
    
    # test input
    is_type(values, (int, float, list), 'values')
    is_type(max_log10, (int, float), 'max_log10')
    # map to list if needed.
    if not isinstance(values, list):
        values = [values]
    # transform, and replacing zero's
    nlog10 = [-1 * np.log10(v) if v != 0 else max_log10 for v in values]
    # truncating
    nlog10 = [max_log10 if m > max_log10 else m for m in nlog10]
    # returning
    return nlog10


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Formatting function

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sort_table(data_dict:Dict[str, Any],
               sort_column:Union[None,str,List[str]]=None,
               kwargs_data_frame:Union[Dict[Any, Any], None]=None,
               kwargs_sort_value:Union[Dict[Any, Any], None]=None,
               ) -> pd.DataFrame:
    '''
    Maps a dictionary to a pd.DataFrame and sorts the dataframe on the
    `sort_column`, which should match a dictionary key.
    
    Arguments
    ---------
    data_dict: dict
        A dictionary which should be mapped to a `pd.DataFrame`.
    sort_column: string or list of strings, default `NoneType`
        The `data_dict` key(s) which should be used to sort the `pd.DataFrame`.
    kwargs_data_frame: dict
        A dictionary with keyword arguments for `pd.DataFrame`.
    kwargs_sort_value: dict
        A dictionary with keyword arguments for `pd.DataFrame.sort_values`.
    
    Returns
    -------
    frame: pd.DataFrame,
        A sorted `pd.DataFrame` with the `data_dict` values.
        
    
    Examples
    --------
    >>> dict_test = {'name': ['test1', 'test3', 'test4', 'test2'],
                     'value': [1, 3, 4, 2],
                     }
    >>> format_tabular.sort_table(
                    dict_test, sort_column='value',
                    kwargs_sort_value={'ascending':False,
                                          })
        name  value
    0  test4      4
    1  test3      3
    2  test2      2
    3  test1      1
    '''

    # check input
    is_type(sort_column, (type(None), str, list))
    is_type(data_dict, dict)
    is_type(kwargs_data_frame, (type(None), dict), 'kwargs_data_frame')
    is_type(kwargs_sort_value, (type(None), dict), 'kwargs_sort_value')
    # set None to empty dict
    kwargs_data_frame, kwargs_sort_value = _assign_empty_default(
            [kwargs_data_frame, kwargs_sort_value], empty_object=dict,
    )
    # make dataframe
    frame = pd.DataFrame(data=data_dict, **kwargs_data_frame,
                         )
    # sort dataframe
    if not sort_column is None:
        frame = frame.sort_values(by=sort_column, **kwargs_sort_value)
    # rest index
    frame = frame.reset_index(drop=True)
    # return
    return frame


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Standardise_Table(object):
    '''
    A class to clean and format standardise a table of mixed data types.
    
    Parameters
    ----------
    data: `pd.DataFrame`
        The tabular data which needs to be standardised.
    
    Attributes
    ----------
    data: `pd.DataFrame`
        The standardised data.
    
    Methods
    -------
    rename_columns(original_columns, new_columns, drop_original)
        Renames a subset of columns and optionally drops the original columns.
    change_column_values(column_values_dict)
        Change the values of multiple columns.
    strip_column_values(columns)
        Removes unnecessary spaces before or after from column string elements.
    apply_function(func_columns, new_columns, func, func_kwargs)
        Applies a function on a subset of columns using list comprehension.
    calc_minus_log10(original_columns, new_columns, nlog10_kwargs)
        Calculates the -1*log10 values of a subset of columns with numerical
        content
    move_to_front(columns)
        Move `columns` to the front of the table.
    
    Notes
    -----
    The following operations are implemented:
        - Renames column (either retaining or dropping the original columns),
        
        - Mapping old to new column values, by repeatedly calling
          pd.DataFrame.map(dict),
        
        - Stripping white space from string columns,
        
        - Substituting strings,
        
        - Applies an arbitrary function to element wise map the column content
          of any subset of columns in `data`.
        
        - -1*log10 transformations on numerical columns,
        
        - Moves columns towards the front,
    '''
    
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __init__(self, data:pd.DataFrame,):
        '''
        Copies the data internally.
        '''
        is_df(data)
        self.data = data.copy()
    
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __str__(self):
        return f"Standardise_Table instance with data=\n{self.data.__str__()}"
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __repr__(self):
        return f"Standardise_Table(data=\n{self.data.__repr__()})"
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def rename_columns(self,
                       original_columns: List[str],
                       new_columns: List[str],
                       drop_original: bool = False,
                       ):
        '''
        Renames a subset of columns and optionally drops the original columns
        (i.e., copying versus moving).
        
        Parameters
        ----------
        original_columns : `list` [`str`]
            A list of column names.
        new_columns : `list` [`str`]
            The new column names
        drop_original : `bool`, default `False`
            Should the original column names be dropped. Only works when
            `new_columns` names are supplied.
        '''
        # check input
        is_type(original_columns, list, 'original_columns')
        is_type(new_columns, list, 'new_columns')
        is_type(drop_original, bool, 'drop_original')
        # ### check format
        are_columns_in_df(self.data, original_columns)
        if len(original_columns) != len(new_columns):
            raise InputValidationError(
                '`original_columns` and `new_columns` should have the '
                'same number of elements, not {} and {}'.format(
                    len(original_columns), len(new_columns)
                ))
        # run the for loop
        for ocol, ncol in zip(original_columns, new_columns):
            self.data[ncol] = self.data[ocol]
        # do we want to drop the original columns
        if drop_original == True:
            self.data.drop(original_columns, axis=1, inplace=True)
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def change_column_values(self, column_values_dict: Dict[str, dict],
                             ):
        '''
        Change the values of multiple columns using a dictionary formatted as
        follows:
            {`column`: {old_value: new_value}}
        
        Parameters
        ----------
        column_values_dict : `dict`
            A dictionary with the column name as key, and a dictionary{old:new}
            with the desired old and new values.
        '''
        
        # check input
        is_type(column_values_dict, dict, 'column_values_dict')
        # ### renaming column values
        for key, value in column_values_dict.items():
            self.data[key] = self.data[key].map(value)
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def strip_column_values(self, columns: List[str],
                            ):
        '''
        Removes unnecessary spaces before or after from column string elements.
        
        Parameters
        ----------
        strip_columns : `list` [`str`]
            The column names for which the unnecessary spaces from the strings
            should be stripped. The index names can be stripped by including
            `index`.
        '''
        is_type(columns, list, 'columns')
        # do we need to strip the index
        if TableNames.index in columns:
            self.data.index = self.data.index.str.strip()
            # remove index from strop_columns
            columns = [s for s in columns if s != TableNames.index]
        # strip the remaining
        if len(columns) > 0:
            for col in columns:
                self.data[col] = self.data[col].str.strip()
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def apply_function(self, func_columns: List[str],
                       new_columns: Union[List[str], None] = None,
                       func: FunctionType = nlog10_func,
                       func_kwargs: Union[Dict[Any, Any], None]=None,
                       ):
        '''
        Applies a function on a subset of columns using list comprehension.
        
        Parameters
        ----------
        funct_columns: `list` [`str`]
            A list of column names to which `func` should be applied.
        new_columns : `list` [`str`], default `NoneType`
            Optional new column names, set to `NoneType` to overwrite
            the original columns.
        func : `FunctionType`, default `nlog10_func`
            The function that should be applied to each column using list
            comprehension. Applies a -1*log10 transformation by default.
        func_kwargs : `dict`
            Optional kwargs supplied to `func`.
        '''
        
        # check input
        is_type(func_columns, list, 'func_columns')
        is_type(new_columns, (type(None), list), 'new_columns')
        is_type(func, FunctionType, 'func')
        is_type(func_kwargs, (type(None), dict), 'func_kwargs')
        # check format
        are_columns_in_df(self.data, func_columns)
        # set None to empty dict
        func_kwargs, = _assign_empty_default([func_kwargs],
                                            empty_object=dict,
                                            )
        # do we have new columns
        if new_columns is not None:
            if len(func_columns) != len(new_columns):
                raise InputValidationError(
                    '`func_columns` and `new_columns` should have the '
                    'same number of elements, not {} and {}'.format(
                        len(func_columns), len(new_columns)
                    ))
        else:
            new_columns = func_columns
        # run the for loop
        for ocol, ncol in zip(func_columns, new_columns):
            self.data[ncol] = [func(m, **func_kwargs) for m in self.data[ocol]]
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calc_minus_log10(self, original_columns: List[str],
                         new_columns: Union[List[str], None] = None,
                         nlog10_kwargs: Union[None, Dict[Any, Any]]=None,
                         ):
        '''
        Calculates the -1*log10 values of a subset of columns with a numerical
        content, and optionally stores these into `new_columns`, or otherwise
        overwrites the original columns.
        
        Parameters
        ----------
        funct_columns: `list` [`str`]
            A list of column names to which `func` should be applied.
        new_columns : `list` [`str`], default `NoneType`
            Optional new column names, set to `NoneType` to overwrite
            the original columns.
        '''
        # set None to empty dict
        nlog10_kwargs, = _assign_empty_default(
            [nlog10_kwargs], empty_object=dict,
        )
        # apply function
        self.apply_function(func_columns=original_columns,
                            new_columns=new_columns,
                            func=nlog10_func,
                            func_kwargs=nlog10_kwargs,
                            )
        # unlist the values
        if new_columns is None:
            new_columns = original_columns
        for col in new_columns:
            self.data[col] = self.data[col].apply(pd.Series)
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def move_to_front(self, columns: List[str],
                      ):
        '''
        Move `columns` to the front of the table.
        
        Parameters
        ----------
        columns: `list` [`str`]
            The column names which should be moved to the front of the table.
        '''
        is_type(columns, list, 'columns')
        # looing over the reverse order of the list
        for p in columns[::-1]:
            self.data.insert(0, p, self.data.pop(p))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_supplementary_tables(tables_to_write: Dict[str, pd.DataFrame],
                               output_file_path: str, sheet_prefix: str = 'S',
                               **kwargs:Optional[Any]):
    """
    Given a dictionary of pandas dataframes and corresponding descriptions,
    write them to an Excel file with one sheet per table, and a final sheet
    with the descriptions.
    
    Parameters
    ----------
    tables_to_write: dict of pd.DataFrame
        A dictionary where each dataframe is a supplementary table, and the
        string is a description of the table.  output_file_path: str, the path
        to write the Excel file to.
    sheet_prefix: str
        The prefix to use for each sheet name.
    kwargs: any
        Additional keyword arguments to pass to pd.DataFrame.to_excel,
        e.g. `index`.
    """
    
    with (pd.ExcelWriter(output_file_path) as writer):
        table_descriptions = {idx + 1: desc for (idx, desc) in
                              enumerate(tables_to_write.keys())}
        pd.DataFrame.from_dict(
            table_descriptions,
            orient='index',
            columns=['Description']).to_excel(writer,
                                              sheet_name='Descriptions',
                                              index=True)
        counter = 1
        for description, table in tables_to_write.items():
            table.to_excel(writer, sheet_name=f'{sheet_prefix}{counter}',
                           **kwargs)
            counter += 1
