'''
Module to work with missing data.
'''
import pandas as pd
from clean_data.constants import (
    MissingNames as MNames,
)
from clean_data.errors import (
    is_type,
    is_df,
)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# constants
MSG1 = '`{}` is a proportion and should not take value: `{}`'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class MissingData(object):
    '''
    Evaluate and filter a pandas table on missing rows or columns.
    
    Parameters
    ----------
    data : `pd.DataFrame`
        A dataframe with `numerical` data.
    verbose : `bool`, default `True`
        Whether warnings should be printed.
    
    Attributes
    ----------
    data : `pd.DataFrame`
        A copy of the inputed data.
    missings_per_row : `pd.Series`
        The number of missings per row as well as the row indices.
    missings_per_column : `pd.Series`
        The number of missings per column as well as the column indices.
    missings_per_row_prop : `pd.Series`
        The proportion of missings per row as well as the row indices.
    missings_per_column_prop : `pd.Series`
        The proportion of missings per column as well as the column indices.
    
    Methods
    -------
    filter(threshold, negate, axis)
    '''
    # /////////////////////////////////////////////////////////////////////////
    def __init__(self, data:pd.DataFrame,
                 verbose:bool = True,
                 ) -> None:
        '''
        Initialises a `MissingData` instance.
        
        '''
        # ### Checking input
        is_df(data)
        # ### calculating the missingness per row and column
        row_n, col_n = data.shape
        col_mc = data.isnull().sum(axis=1).sort_values(ascending=False)
        row_mc = data.isnull().sum(axis=0).sort_values(ascending=False)
        missing_dict = {
            MNames.ROW_N : row_n,
            MNames.COL_N : col_n,
            MNames.M_COL_N : col_mc,
            MNames.M_ROW_N : row_mc,
            MNames.M_COL_P : col_mc/col_n,
            MNames.M_ROW_P : row_mc/row_n,
        }
        # ### assing things to self
        setattr(self, MNames.DATA, data)
        setattr(self, MNames.MISSINGS, missing_dict)
    # /////////////////////////////////////////////////////////////////////////
    @property
    def data(self):
        """The data attribute."""
        return getattr(self, MNames.DATA)
    # /////////////////////////////////////////////////////////////////////////
    @property
    def missings_per_column_prop(self):
        """Proportion of missingness per column."""
        GET = MNames.M_ROW_P
        return getattr(self, MNames.MISSINGS)[GET]
    # /////////////////////////////////////////////////////////////////////////
    @property
    def missings_per_row_prop(self):
        """Proportion of missingness per row."""
        GET = MNames.M_COL_P
        return getattr(self, MNames.MISSINGS)[GET]
    # /////////////////////////////////////////////////////////////////////////
    @property
    def missings_per_column(self):
        """Number of missings per column."""
        GET = MNames.M_ROW_N
        return getattr(self, MNames.MISSINGS)[GET]
    # /////////////////////////////////////////////////////////////////////////
    @property
    def missings_per_row(self):
        """Number of missings per row."""
        GET = MNames.M_COL_N
        return getattr(self, MNames.MISSINGS)[GET]
    # /////////////////////////////////////////////////////////////////////////
    def filter(self, threshold:float, negate:bool=False, axis:int=0,
               ) -> list[int | float]:
        """
        Extract the column or row indices with greater or equal proportion of
        missingness compared to the threshold.
        
        Parameters
        ----------
        threshold : `float`
            The proprotion of missigness. Should be between 0 and 1.
        negate : `bool`, default `False`
            Whether the return the indices with less missigness than the
            threshold.
        axis : `int`, default 1
            Whether to return the column indices (axis=1) or the row indices
            (axis=0).
        
        Return
        ------
        indices : list [`float` or `int`]
            The row or column indices.
        """
        is_type(negate, bool)
        is_type(axis, int)
        is_type(threshold, float)
        if threshold < 0 or threshold > 1:
            raise ValueError(MSG1.format('threshold', threshold))
        if not axis in [0, 1]:
            raise ValueError('`axis` is limited to 0 or 1.')
        if axis == 1:
            tab = getattr(self, MNames.MISSINGS)[MNames.M_ROW_P]
        else:
            tab = getattr(self, MNames.MISSINGS)[MNames.M_COL_P]
        if negate == False:
            idx = tab[tab >= threshold].index.to_list()
        else:
            idx = tab[tab <= threshold].index.to_list()
        # return
        return idx


