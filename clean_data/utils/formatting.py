#!/usr/bin/env python3
'''
A module packaging various (string) formatting functions.

Functions directly working on tabular data are available through
`clean_data.format_tabular`. The current module focusses on one off functions
which may be useful to handle a single value or a collection of values, not
necessarily recorded in a table.
'''
import warnings
import numpy as np
from numbers import Real
from scipy.stats import norm
from typing import Any, List, Type, Union, Tuple, Dict, ClassVar, Optional
from clean_data.errors import (
    is_type,
)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def format_estimates(point:float, se:Union[float,None]=None,
                      lower:Union[float, None]=None,
                      upper:Union[float, None]=None, alpha:float=0.05,
                      round:int=2, exp:bool=False
                      ) -> str:
    """
    Formats point estimates with confidence intervals
    
    Parameters
    ----------
    point, se, lower, upper : float
        Please supply either the `se` or (`lower` and `upper`) as floats.
    alpha : float, default 0.05
        The desired type 1 error rate
    round : integer, default 2
        The desired number of significant figures
    exp : boolean, default False
        Should the point estimates, lower and upper bounds be exponentiated
        with base `e`
    
    Returns
    -------
    formatted string : str
        `point (lower; upper)` formatted string with appropriate rounding
    """
    # check input
    is_type(round, int)
    is_type(alpha, float)
    is_type(point, float)
    if se == None and not ( isinstance(lower, float) and isinstance(upper, float) ):
            raise TypeError('Please supply either an `se`, or both  `lower` '
                            'and `upper`.')
    if isinstance(se, float) and not ( lower == None and upper == None):
            raise TypeError('Please supply either an `se`, or both  `lower` '
                            'and `upper`.')
    if isinstance(se, float) and isinstance(lower, float) and\
    isinstance(upper, float):
            warnings.warn('Ignoring `se`.', SyntaxWarning)
    # calculate lower and upper bounds
    if (lower == None and upper == None):
        z = norm.ppf(1-alpha/2)
        lower = point - z*se
        upper = point + z*se
    if exp == True:
        point = np.exp(point)
        upper = np.exp(upper)
        lower = np.exp(lower)
    # format string
    r_format = '.{}f'.format(round)
    format_string = format(point, r_format) + ' (' + format(lower, r_format) + \
        '; ' + format(upper, r_format) + ')'
    return format_string

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _superscriptinate(number:str) -> str:
    '''
    Will replace any number (0-9), seperators, and addition substraction
    sumbols with a superscript equivalent expression.
    
    Parameters
    ----------
    number : `str`
    
    Returns
    -------
    number : `str`
        A string with superscript numbers.
    '''
    return number.replace('0','⁰').replace('1','¹').replace('2','²').\
        replace('3','³').replace('4','⁴').replace('5','⁵').replace('6','⁶')\
        .replace('7','⁷').replace('8','⁸').replace('9','⁹').replace('-','⁻')\
        .replace('+', '⁺').replace('.','·').replace(',','˒')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _subscriptinate(number:str) -> str:
    '''
    Will replace any number (0-9), seperators, and addition substraction
    sumbols with a subscript equivalent expression.
    
    Parameters
    ----------
    number : `str`
    
    Returns
    -------
    number : `str`
        A string with superscript numbers.
    '''
    return number.replace('0','₀').replace('1','₁').replace('2','₂').\
        replace('3','₃').replace('4','₄').replace('5','₅').replace('6','₆')\
        .replace('7','₇').replace('8','₈').replace('9','₉').replace('-','₋')\
        .replace('+', '₊').replace('.','.').replace(',','ˏ')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sci_notation(number:float, sig_fig:int=2,
                 max:float=np.float_power(10, -100)) -> str:
    """
    Returns a number in scientific notation with the lead numbers to  a
    specific significant number `sig_fig`
    
    Automatically truncates values if too small to print.

    Parameters
    ----------
    number : `float`
        A number as float or integer.
    sig_fig : `int`
        The number of significant numbers after the decimial point.
    max `float`
        the float value above which values get truncated to the max
        (i.e., winsorised)
    
    Returns
    -------
    number: str
        A number in scientific notation.
    
    Examples
    --------
    >>> sci_notation(2465640, sig_fig=4)
    '2.4656×10⁶
    """
    if number < max:
        number = max
    # getting string
    ret_string = "{0:.{1:d}e}".format(number, sig_fig)
    try:
        a,b = ret_string.split("e")
        # removed leading "+" and strips leading zeros too.
        b = int(b)
        return a + "×10" + _superscriptinate(str(b))
    except ValueError or TypeError:
        return str(np.nan)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def super_subs_notation(text):
    '''
    Replaces a ^{number} by the superscript equivalent statement, and the same
    for _{number} but returning subscript.
    
    Parameters
    ----------
    text : `str`
        Any string containing ^{number} or _{number}.
    
    Returns
    -------
    newtext : `str`
        An updated string with super and subscripts.
    
    Examples
    --------
    >>> text = 'this is a string with log_{10}(x), log_{2}{10}, and cm^{2} units'
    >>> super_subs_notation(text)
    'this is a string with log₁₀(x), log₂{10}, and cm² units'
    '''
    # ### check input
    is_type(text, str)
    # ### replace superscript
    while '^{' in text:
        start = text.find('^{') + 2
        end = text.find('}', start)
        if end != -1:
            number = text[start:end]
            superscript = _superscriptinate(number)
            text = text[:start - 2] + superscript + text[end + 1:]
    # ### replace subscript
    while '_{' in text:
        start = text.find('_{') + 2
        end = text.find('}', start)
        if end != -1:
            number = text[start:end]
            subscript = _subscriptinate(number)
            text = text[:start - 2] + subscript + text[end + 1:]
    # ### return stuff
    return text

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_numerics(first_value: Real,
                    second_value: Real,
                    third_value: Real | None=None,
                    brackets: list[str] | None  = [' (', ')'],
                    delimiter: str | None = '; ',
                    round:int=2,
                    round_within_brackets: int | None =None,
                    exp:bool=False,
                    ) -> str:
    """
    Creates a string with 2-3 values, where either the second values is
    between `brackets` or the second and thirds values are within brackets
    with a `delimiter`.
    
    Arguments
    ---------
    .*_value : `float` or `int`,
        The third value is optional and can be skipped using `NoneType`.
    brackets : `list` of two `strings`, default `[' (', ')']`
        Two strings used to surround the second/third value.
    delimiter : `str`, default '; '
        The delimiter used to seperate the second and thrid value.
    round : `int`, default 2
        The desired rounding.
    round_within_brackets : `int`, default None
        An optional rounding for the value within brackets. Set to `Nonetype`
        to use `round` instead.
    exp : `bool`, default `False`
        Whether the values should be mapped through the `exponential` function.
    
    Returns
    -------
    format_string : `str`
        A formated string with either `number (number)` or
        `number (number; number)` formatting.
    """
    # check input
    is_type(first_value, (float, int))
    is_type(second_value, (float, int))
    is_type(third_value, (float, int, type(None)))
    is_type(round_within_brackets, (int, type(None)))
    # do we exp-transform values
    if exp == True:
        first_value = np.exp(first_value).item()
        second_value = np.exp(second_value).item()
        if not third_value is None:
            third_value = np.exp(third_value).item()
    # format string
    r_format = '.{}f'.format(round)
    if not round_within_brackets is None:
        r_format2 = '.{}f'.format(round_within_brackets)
    else:
        r_format2 = r_format
    if not third_value is None:
        format_string = format(first_value, r_format) + brackets[0] + \
            format(second_value, r_format2) + delimiter + \
            format(third_value, r_format2) + brackets[1]
    else:
        format_string = format(first_value, r_format) + brackets[0] + \
            format(second_value, r_format2) + brackets[1]
    return format_string

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def inq_notation(number:float, threshold = 0.001, round:int=3) -> str:
    '''
    Returns a `smaller than` inequality for `numbers` smaller than `threshold`
    and a string formatted number otherwise.
    
    Parameters
    ----------
    number: `float`
        A number as float or integer.
    threshold : `float`, default 0.001
        The threshold below which a `number` will be returned as inequality.
    round : `int`, default 3
        The desired rounding, typically this should be the same amount of
        dp as used in threshold.
    
    Returns
    -------
    number: str
        A number in scientific notation.
    
    Notes
    -----
    Basically this returns an f" < {threshold}" or applies an
    '.{}f'.format(round).
    
    Examples
    --------
    >>> inq_notation(0.0001, 0.001)
    ' < 0.001'
    '''
    if np.isnan(number) == False:
        if number < threshold:
            string_num = f" < {threshold}"
        else:
            string_num = format(number, '.{}f'.format(round))
    else:
        string_num = np.nan
    return string_num

