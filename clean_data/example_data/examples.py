"""Provides centralised access to example data sets that can be used in tests
and also in example code and/or jupyter notebooks.

The `skeleton_package.example_data.examples` module is very simple, this is
not really designed for editing via end users but they should call the three
public functions, `skeleton_package.example_data.examples.get_data()`,
`skeleton_package.example_data.examples.help()` and
`skeleton_package.example_data.examples.list_datasets()`.

Notes
-----
Data can be "added" either through functions that generate the data on the fly
or via functions that load the data from a static file located in the
``example_data`` directory. The data files being added  should be as small as
possible (i.e. kilobyte/megabyte range). The dataset functions should be
decorated with the ``@dataset`` decorator, so the example module knows about
them. If the function is loading a dataset from a file in the package, it
should look for the path in ``_ROOT_DATASETS_DIR``.

Examples
--------

Registering a function as a dataset providing function:

>>> @dataset
>>> def dummy_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that returns a small list.
>>>
>>>     Returns
>>>     -------
>>>     data : `list`
>>>         A list of length 3 with ``['A', 'B', 'C']``
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions.
>>>     \"\"\"
>>>     return ['A', 'B', 'C']

The dataset can then be used as follows:

>>> from skeleton_package.example_data import examples
>>> examples.get_data('dummy_data')
>>> ['A', 'B', 'C']

A dataset function that loads a dataset from file, these functions should load
 from the ``_ROOT_DATASETS_DIR``:

>>> @dataset
>>> def dummy_load_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that loads a string from a file.
>>>
>>>     Returns
>>>     -------
>>>     str_data : `str`
>>>         A string of data loaded from an example data file.
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions. The path to this dataset is
>>>     built from ``_ROOT_DATASETS_DIR``.
>>>     \"\"\"
>>>     load_path = os.path.join(_ROOT_DATASETS_DIR, "string_data.txt")
>>>     with open(load_path) as data_file:
>>>         return data_file.read().strip()

The dataset can then be used as follows:

>>> from skeleton_package.example_data import examples
>>> examples.get_data('dummy_load_data')
>>> 'an example data string'
"""
import os
import re
import pandas as pd
import numpy as np
from scipy import stats
from typing import Any, List, Type, Union, Tuple, Optional, Dict

# The name of the example datasets directory
_EXAMPLE_DATASETS = "example_datasets"
"""The example dataset directory name (`str`)
"""

_ROOT_DATASETS_DIR = os.path.join(os.path.dirname(__file__), _EXAMPLE_DATASETS)
"""The root path to the dataset files that are available (`str`)
"""

_DATASETS = dict()
"""This will hold the registered dataset functions (`dict`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dataset(func):
    """Register a dataset generating function. This function should be used as
    a decorator.
    
    Parameters
    ----------
    func : `function`
        The function to register as a dataset. It is registered as under the
        function name.
    
    Returns
    -------
    func : `function`
        The function that has been registered.
    
    Raises
    ------
    KeyError
        If a function of the same name has already been registered.
    
    Notes
    -----
    The dataset function should accept ``*args`` and ``**kwargs`` and should be
    decorated with the ``@dataset`` decorator.
    
    Examples
    --------
    Create a dataset function that returns a dictionary.
    
    >>> @dataset
    >>> def get_dict(*args, **kwargs):
    >>>     \"\"\"A dictionary to test or use as an example.
    >>>
    >>>     Returns
    >>>     -------
    >>>     test_dict : `dict`
    >>>         A small dictionary of string keys and numeric values
    >>>     \"\"\"
    >>>     return {'A': 1, 'B': 2, 'C': 3}
    >>>
    
    The dataset can then be used as follows:
    
    >>> from skeleton_package.example_data import examples
    >>> examples.get_data('get_dict')
    >>> {'A': 1, 'B': 2, 'C': 3}
    
    """
    try:
        _DATASETS[func.__name__]
        raise KeyError("function already registered")
    except KeyError:
        pass
    
    _DATASETS[func.__name__] = func
    return func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_data(name, *args, **kwargs):
    """Central point to get the datasets.
    
    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a registered
        dataset function.
    *args
        Arguments to the data generating functions
    **kwargs
        Keyword arguments to the data generating functions
    
    Returns
    -------
    dataset : `Any`
        The requested datasets
    """
    try:
        return _DATASETS[name](*args, **kwargs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def list_datasets():
    """List all the registered datasets.
    
    Returns
    -------
    datasets : `list` of `tuple`
        The registered datasets. Element [0] for each tuple is the dataset name
        and element [1] is a short description captured from the docstring.
    """
    datasets = []
    for d in _DATASETS.keys():
        desc = re.sub(
            r'(Parameters|Returns).*$', '', _DATASETS[d].__doc__.replace(
                '\n', ' '
            )
        ).strip()
        datasets.append((d, desc))
    return datasets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def help(name):
    """Central point to get help for the datasets.
    
    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a unique key in the
        DATASETS module level dictionary.
    
    Returns
    -------
    help : `str`
        The docstring for the function.
    """
    docs = ["Dataset: {0}\n{1}\n\n".format(name, "-" * (len(name) + 9))]
    try:
        docs.extend(
            ["{0}\n".format(re.sub(r"^\s{4}", "", i))
             for i in _DATASETS[name].__doc__.split("\n")]
        )
        return "".join(docs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# simulate a flat dataset
@dataset
def patient_characteristics(nrow:int=10,
                            ncontinous_variables:int=1,
                            nbinary_variables:int=1,
                            ncategorical_variables:int=1,
                            nstring_variables:int=0,
                            seed:int=1986,
                            string_list:List[str]=\
                            ['string' + str(i) for i in range(4)],
                            prop_nan:float = 0,
                            kw_norm:Dict[Any, Any]={'loc':0, 'scale':1},
                            kw_binom:Dict[Any, Any]={'n':1, 'p':0.5},
                            kw_run:Dict[Any, Any]={'low':1, 'high':5},
                            ) -> pd.DataFrame:
    '''
    Generate a random number of simulated data. Returns a pd.DataFrame with
    `nrow` and columns labeles `x` followed by a sequential numerical starting
    at 1.
    
    Parameters
    ----------
    nrow : `int`, default 4
        The number of rows to be returned.
    ncontinous_variables : `int`, default 1
        The number of columns with data sampled from a standard normal
        distribution. Set to 0 to skip.
    nbinary_variables : `int`, default 1
        The number of columns with data sampled from a binomial distribution.
        Set to 0 to skip.
    ncategorical_variables : `int`, default 1
        The number of columns with data sampled from a discrite uniform
        distribution. Set to 0 to skip.
    nstring_variables : `int`, default 0
        The number of columns with data string data. Will be sampled from
        `string_list`. Set to 0 to skip.
    string_list : `list` [`str`]
        A list of string that can be sampled from.
    prop_nan : `float`, default 0
        The proportion of missing observations.
    seed : int, default 1986
        The random seed.
    kw_norm : `dict` [`any`, `any`]
        A dictionary with arguments to `scipy.stats.norm.rvs`.  Optional.
    kw_binom : `dict` [`any`, `any`]
        A dictionary with arguments to `scipy.stats.binom.rvs`.  Optional.
    kw_run : `dict` [`any`, `any`]
        A dictionary with arguments to `scipy.stats.randint.rvs`.  Optional.
    '''
    # set seed
    np.random.seed(seed)
    # standard scipy arguments, just in case you want to add stuff without
    # overwriting the defaults
    norm_kwargs = {'loc':0, 'scale':1}
    norm_kwargs.update(kw_norm)
    binom_kwargs = {'n':1, 'p':0.5}
    binom_kwargs.update(kw_binom)
    run_kwargs = {'low':0, 'high':5}
    run_kwargs.update(kw_run)
    # generate continous variables
    col_suffix=1
    dict_continous=\
        {'x'+str(i+col_suffix): stats.norm.rvs(size=nrow, **norm_kwargs) for i\
         in range(ncontinous_variables)}
    # update column name numeric
    col_suffix=col_suffix + ncontinous_variables
    # generate binary variables
    dict_binary=\
        {'x'+str(i+col_suffix): stats.binom.rvs(size=nrow, **binom_kwargs) \
         for i in range(nbinary_variables)}
    # update column name numeric
    col_suffix=col_suffix + nbinary_variables
    # generate categorical variables
    dict_categorical=\
        {'x'+str(i+col_suffix): stats.randint.rvs(size=nrow, **run_kwargs) \
         for i in range(ncategorical_variables)}
    # update column name string
    col_suffix=col_suffix + ncategorical_variables
    # generate string variables
    dict_string=\
        {'x'+str(i+col_suffix): np.random.choice(string_list, nrow) \
         for i in range(nstring_variables)}
    # make data frame
    data = pd.concat(
        [pd.DataFrame.from_dict(dict_continous, orient='columns'),
         pd.DataFrame.from_dict(dict_binary, orient='columns'),
         pd.DataFrame.from_dict(dict_categorical, orient='columns'),
         pd.DataFrame.from_dict(dict_string, orient='columns'),
         ],
        axis=1,
    )
    # do we need to add missing data
    if prop_nan > 0.0:
        num_nan = int(data.size * prop_nan)
        # flatten and nan
        random_indices = np.random.choice(data.size, num_nan, replace=False)
        data_flat = data.to_numpy().flatten()
        data_flat[random_indices] = np.nan
        # turn to dataframe again
        data = pd.DataFrame(data_flat.reshape(data.shape),
                              columns=data.columns,
                              index=data.index)
    # return
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def crude_follow_up_data(*args, **kwargs):
    """
    A pandas.DataFrame containing information on the dates of `start` and
    `stop` of follow-up, as well as dates of `event` and `death.
    
    The data is `crude` in the sense that some dates are missing, or rows with
    a `start` date after the `stop` date, and similarly an `event` date after
    the `death` date.
    
    Returns
    -------
    data : pd.DataFrame
        A DataFrame with the aforementioned columns.
    
    """
    data = pd.DataFrame(*args, data={
        'start': [ '28-7-2000', '15-2-2013', '23-4-2012', '18-7-2016',
                  '28-1-2013', '19-1-2016',
                  ],
        'stop': [ '28-2-2009', '15-2-2019', '14-9-2018', '25-10-2018',
                 '15-3-2019', np.nan,
                 ],
        'event': ['6-9-2012', '10-2-2019', '10-4-2014', np.nan, '25-1-2019',
                  '20-1-2016',
                  ],
        'death': [np.nan, '11-2-2019', '25-3-2015', '20-10-2017', '20-2-2018',
                  '18-2-2018',
                  ],
        'event_bin': [1, 1, 1, 0, 1, 1],
        'death_bin': [0, 1, 1, 1, 1, 1],
        
        }, **kwargs)
    # return
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def load_table_data(*args, **kwargs):
    """
    Loads MR data for a SAP against many outcomes. Can be used as testing
    data for table manipulations.
    
    Returns
    -------
    pd.DataFrame
    """
    # files
    df = pd.read_csv(
        os.path.join(_ROOT_DATASETS_DIR, 'table_data.tsv.gz'),
        sep='\t', index_col=0
    )
    # return
    return df


